package cz.vutbr.fit.upa;

import cz.vutbr.fit.upa.exception.ConnectionException;
import cz.vutbr.fit.upa.model.ConnectionData;
import oracle.jdbc.pool.OracleDataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for connecting to database and more. Also returns instance because it is singleton.
 */
public class Database {

    /**
     * Logger for logging
     */
    private static final Logger log = LoggerFactory.getLogger(Database.class);

    /**
     * Oracle data source for connection data
     */
    private OracleDataSource oracleDataSource;

    /**
     * Connection for queries
     */
    private Connection connection;

    /**
     * Instance of class
     */
    private static Database instance = null;

    /**
     * Creates or returns instance.
     *
     * @return instance of database
     */
    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
        }
        return instance;
    }

    /**
     * Getter for connection.
     *
     * @return connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Connect to database with connection data.
     *
     * @param connectionData username, password, host, url, service name and port
     * @return true if connected successful, false if not
     */
    public boolean connectToDatabase(ConnectionData connectionData) {
        try {
            String url = "jdbc:oracle:thin:@//" +
                    connectionData.getHost() +
                    ":" + connectionData.getPort() +
                    "/" + connectionData.getServiceName();

            this.oracleDataSource = new OracleDataSource();
            this.oracleDataSource.setURL(url);
            setConnectionCredentials(connectionData);
            this.connection = this.oracleDataSource.getConnection();
            log.info("Connection was successful.");
        } catch (Exception ex) {
            log.error("Error in connection. Thrown exception is: " + ex.getMessage());
            throw new ConnectionException(ex.getMessage());
        }
        return true;
    }

    /**
     * Sets credentials of user to oracle data source.
     *
     * @param connectionData data
     */
    public void setConnectionCredentials(ConnectionData connectionData) {
        this.oracleDataSource.setUser(connectionData.getUsername());
        this.oracleDataSource.setPassword(connectionData.getPassword());
    }

    /**
     * Sends content of sql script as query to database
     *
     * @param sqlFileToSend sql file
     */
    public void sendSqlToDatabase(List<String> sqlFileToSend) {
        for (final String sqlexec : sqlFileToSend) {
            try {
                Statement stmt = this.connection.createStatement();
                try {
                    ResultSet rset;
                    try {
                        rset = stmt.executeQuery(sqlexec);
                        rset.close();
                    } catch (SQLException ex) {
                        log.error("Sql statement was not executed successfully. Thrown exception: " + ex.getMessage());
                    }

                } finally {
                    stmt.close();
                }
            } catch (SQLException ex) {
                log.error("Sql statement was not executed successfully. Thrown exception: " + ex.getMessage());
            }
        }
    }
}
