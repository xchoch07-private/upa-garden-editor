package cz.vutbr.fit.upa;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Class for showing photos (similar and in full size) in new frame.
 */
public class PhotoAlertBox {

    /**
     * Static method which shows image in new frame.
     *
     * @param image image to show
     * @param title title of new frame
     */
    public static void displayPhoto(Image image, String title) {
        Stage stage = new Stage();

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle(title);
        stage.setResizable(false);
        stage.setWidth(image.getWidth());
        stage.setHeight(image.getHeight());

        AnchorPane pane = new AnchorPane();
        Scene scene = new Scene(pane, image.getWidth(), image.getHeight());

        BackgroundSize backgroundSize = new BackgroundSize(image.getWidth(), image.getHeight(), false, false, true, false);
        BackgroundImage background = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                backgroundSize);
        pane.setBackground(new Background(background));

        EventHandler<MouseEvent> eventHandler = e -> {
            stage.close();
        };
        pane.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler);

        stage.setScene(scene);
        stage.showAndWait();
    }
}
