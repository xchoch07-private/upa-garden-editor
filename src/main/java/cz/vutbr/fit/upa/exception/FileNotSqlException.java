package cz.vutbr.fit.upa.exception;

/**
 * This exception is thrown when chosen file as initial script has not sql format.
 */
public class FileNotSqlException extends RuntimeException {

    public FileNotSqlException() {
        super();
    }
}
