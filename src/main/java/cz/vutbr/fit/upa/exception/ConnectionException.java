package cz.vutbr.fit.upa.exception;

/**
 * This exception is thrown when connection did not proceed correctly.
 */
public class ConnectionException extends RuntimeException {

    public ConnectionException(String message) {
        super(message);
    }
}
