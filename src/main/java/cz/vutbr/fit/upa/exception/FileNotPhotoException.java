package cz.vutbr.fit.upa.exception;

/**
 * This exception is thrown when chosen file for photo has not a png or jpg format.
 */
public class FileNotPhotoException extends RuntimeException {

    public FileNotPhotoException() {
        super();
    }
}
