package cz.vutbr.fit.upa.exception;

/**
 * This exception is thrown when sql statement not executed correctly.
 */
public class SqlStatementNotExecuted extends RuntimeException {

    public SqlStatementNotExecuted() {
        super();
    }
}
