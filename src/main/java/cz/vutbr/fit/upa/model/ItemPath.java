package cz.vutbr.fit.upa.model;

import javafx.scene.shape.Path;

/**
 * Class representing database item of shape path.
 */
public class ItemPath extends Path {

    /**
     * Reference to database item which represents item of database
     */
    private DatabaseItem itemPathReference;


    /**
     * Id of this item
     */
    private int itemIdFromDatabase;

    /**
     * Constructor for creating item path.
     *
     * @param itemPathReference  item reference
     * @param itemIdFromDatabase id
     */
    public ItemPath(DatabaseItem itemPathReference, int itemIdFromDatabase) {
        super();
        this.itemPathReference = itemPathReference;
        this.itemIdFromDatabase = itemIdFromDatabase;
    }

    /**
     * Getter for database item reference
     *
     * @return item reference
     */
    public DatabaseItem getItemPathReference() {
        return itemPathReference;
    }

    /**
     * Setter for item polygon reference
     *
     * @param itemPathReference reference
     */
    public void setItemPathReference(DatabaseItem itemPathReference) {
        this.itemPathReference = itemPathReference;
    }

    /**
     * Getter for id.
     *
     * @return id
     */
    public int getItemIdFromDatabase() {
        return itemIdFromDatabase;
    }

    /**
     * Setter for id.
     *
     * @param itemIdFromDatabase
     */
    public void setItemIdFromDatabase(int itemIdFromDatabase) {
        this.itemIdFromDatabase = itemIdFromDatabase;
    }
}
