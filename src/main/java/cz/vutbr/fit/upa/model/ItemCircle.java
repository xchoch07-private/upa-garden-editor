package cz.vutbr.fit.upa.model;

import javafx.scene.shape.Circle;

/**
 * Class representing database item of shape circle.
 */
public class ItemCircle extends Circle {

    /**
     * Reference to database item which represents item of database
     */
    private DatabaseItem itemCircleReference;

    /**
     * Id of this item
     */
    private int itemIdFromDatabase;

    /**
     * Constructor for creating item circle.
     *
     * @param itemReference      item reference
     * @param itemIdFromDatabase id
     */
    public ItemCircle(DatabaseItem itemReference, int itemIdFromDatabase) {
        super();
        this.itemCircleReference = itemReference;
        this.itemIdFromDatabase = itemIdFromDatabase;
    }

    /**
     * Getter for database item reference
     *
     * @return item reference
     */
    public DatabaseItem getItemCircleReference() {
        return itemCircleReference;
    }

    /**
     * Setter for item polygon reference
     *
     * @param itemCircleReference reference
     */
    public void setItemCircleReference(DatabaseItem itemCircleReference) {
        this.itemCircleReference = itemCircleReference;
    }

    /**
     * Getter for id.
     *
     * @return id
     */
    public int getItemIdFromDatabase() {
        return itemIdFromDatabase;
    }

    /**
     * Setter for id.
     *
     * @param itemIdFromDatabase
     */
    public void setItemIdFromDatabase(int itemIdFromDatabase) {
        this.itemIdFromDatabase = itemIdFromDatabase;
    }
}
