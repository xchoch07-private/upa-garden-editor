package cz.vutbr.fit.upa.model;

import oracle.spatial.geometry.JGeometry;

/**
 * Class which represents item from database of canvas items. It has all properties as item in database.
 */
public class DatabaseItem {

    /**
     * Id of item
     */
    public int id;

    /**
     * Name of item
     */
    public String name;

    /**
     * Note of item
     */
    public String note;

    /**
     * Geometry of shape which is represented by this item
     */
    public JGeometry geometry;

    /**
     * Type of type (camera, house, path, ...)
     */
    private String itemType;

    /**
     * Foreign key id photo of camera, if it has any
     */
    private int idPhoto;

    /**
     * Constructor which creates instance of database item.
     *
     * @param id       id of item
     * @param name     name of item
     * @param note     note of item
     * @param geometry geometry of shape of item
     * @param itemType type of item
     * @param idPhoto  id photo of camera if has any
     */
    public DatabaseItem(int id, String name, String note, JGeometry geometry, String itemType, int idPhoto) {
        this.id = id;
        this.name = name;
        this.note = note;
        this.geometry = geometry;
        this.itemType = itemType;
        this.idPhoto = idPhoto;
    }

    /**
     * Getter for id
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Get name of item
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Get note of item
     *
     * @return note
     */
    public String getNote() {
        return note;
    }

    /**
     * Gets geometry of item
     *
     * @return geometry
     */
    public JGeometry getGeometry() {
        return geometry;
    }

    /**
     * Gets item type of item
     *
     * @return item type
     */
    public String getItemType() {
        return itemType;
    }

    /**
     * Gets id photo if has any
     *
     * @return id
     */
    public int getIdPhoto() {
        return idPhoto;
    }

    /**
     * Sets id photo of camera if needed
     *
     * @param idPhoto id photo to set
     */
    public void setIdPhoto(int idPhoto) {
        this.idPhoto = idPhoto;
    }
}
