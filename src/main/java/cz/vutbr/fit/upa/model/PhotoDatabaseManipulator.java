package cz.vutbr.fit.upa.model;

import cz.vutbr.fit.upa.AlertBox;
import cz.vutbr.fit.upa.Database;
import cz.vutbr.fit.upa.constants.SQLCommands;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import oracle.ord.im.OrdImage;

import java.io.IOException;
import java.sql.*;

/**
 * Manipulates with photo database, prepares and executes queries.
 */
public class PhotoDatabaseManipulator {

    /**
     * Connection to database
     */
    private Connection connection;

    /**
     * Saves photo to photo database.
     *
     * @param url    url to save file to
     * @param camera item camera
     * @return OrdImage
     * @throws SQLException
     * @throws IOException
     */
    public OrdImage savePhotoToDb(String url, ItemPolygon camera) throws SQLException, IOException {

        connection = Database.getInstance().getConnection();
        connection.setAutoCommit(false);
        int id = removeActualPhotoOfCamera(camera);
        if (id == 0)
            id = getLastId() + 1;

        doInitPhotoQuery(id);

        OrdImage img = doSelectPhotoByIdQuery(id);
        img.loadDataFromFile(url);
        img.setProperties();

        doSetPhotoUpdateQuery(id, img);

        updateImageWithSIFeatures(id);

        setIdPhotoOfCamera(id, camera);

        connection.commit();
        connection.setAutoCommit(true);

        camera.getItemPolygonReference().setIdPhoto(id);

        return img;
    }

    /**
     * Removes photo of camera if has any from database.
     *
     * @param camera camera to which removes photo
     * @return id of camera
     */
    private int removeActualPhotoOfCamera(ItemPolygon camera) {
        int idPhoto = 0;
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_GET_ID_PHOTO_OF_CAMERA)) {
            try {
                stmt.setInt(1, camera.getItemIdFromDatabase());
                try (ResultSet resultSet = stmt.executeQuery()) {
                    while (resultSet.next()) {
                        idPhoto = resultSet.getInt("id_photo");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } finally {
                stmt.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        deletePhotoById(connection, idPhoto);
        return idPhoto;
    }

    /**
     * Returns photo as OrdImage selected from database by id.
     *
     * @param idPhoto id
     * @return photo as OrdImage
     */
    public OrdImage doSelectPhotoByIdQuery(int idPhoto) {
        connection = Database.getInstance().getConnection();

        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_SELECT_PHOTO_BY_ID)) {
            try {
                stmt.setInt(1, idPhoto);
                try (OracleResultSet resultSet = (OracleResultSet) stmt.executeQuery()) {
                    resultSet.next();
                    return (OrdImage) resultSet.getORAData("photo", OrdImage.getORADataFactory());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    stmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Inits photo with specified id in database.
     *
     * @param idPhoto id
     */
    private void doInitPhotoQuery(int idPhoto) {
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_INIT_NEW_PHOTO)) {
            try {
                stmt.setInt(1, idPhoto);
                stmt.executeUpdate();
            } finally {
                stmt.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets photos properties (id and image) to database. Photo is chosen by id.
     *
     * @param idPhoto id of photo to set
     * @param img     img to set
     */
    private void doSetPhotoUpdateQuery(int idPhoto, OrdImage img) {
        try (OraclePreparedStatement stmt = (OraclePreparedStatement) connection.prepareStatement(SQLCommands.SQL_UPDATE_SET_PHOTO)) {
            try {
                stmt.setORAData(1, img);
                stmt.setInt(2, idPhoto);
                stmt.executeUpdate();
            } finally {
                stmt.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets foreign key of camera photo in items database.
     *
     * @param idPhoto id photo to set
     * @param camera  camera to set foreign key to
     */
    public void setIdPhotoOfCamera(Integer idPhoto, ItemPolygon camera) {
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_UPDATE_ID_PHOTO_OF_CANVAS_ITEM)) {
            try {
                stmt.setObject(1, idPhoto);
                stmt.setObject(2, camera.getItemIdFromDatabase());
                stmt.executeUpdate();
            } finally {
                stmt.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends query to database to get last used id of photo.
     *
     * @return last id in photo database
     */
    private int getLastId() {
        int lastId = 0;
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_GET_MAX_ID_OF_PHOTO)) {
            try (ResultSet resultSet = stmt.executeQuery()) {
                while (resultSet.next()) {
                    lastId = resultSet.getInt("max");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lastId;
    }

    /**
     * Deletes photo by id.
     *
     * @param connection connection to db
     * @param idPhoto    id of photo to delete
     */
    public void deletePhotoById(Connection connection, int idPhoto) {
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_DELETE_PHOTO_FROM_DB)) {
            try {
                stmt.setInt(1, idPhoto);
                stmt.executeUpdate();
            } finally {
                stmt.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Rotates picture by angle in photo database. Photo is taken by id.
     *
     * @param photoId id of photo to rotate
     * @param angle   angle, is used 90, -90, 180
     */
    public void rotatePicture(int photoId, int angle) {
        connection = Database.getInstance().getConnection();

        String query;

        switch ((int) angle) {
            case 90:
                query = SQLCommands.SQL_UPDATE_ROTATE_RIGHT_PHOTO;
                break;
            case -90:
                query = SQLCommands.SQL_UPDATE_ROTATE_LEFT_PHOTO;
                break;
            case 180:
                query = SQLCommands.SQL_UPDATE_ROTATE_TWICE_PHOTO;
                break;
            default:
                return;
        }

        try (OraclePreparedStatement stmt = (OraclePreparedStatement) connection.prepareStatement(query)) {
            try {
                stmt.setInt(1, photoId);
                stmt.setInt(2, photoId);
                stmt.executeUpdate();
            } finally {
                stmt.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Makes operation mirror on picture in photo database. Photo is taken by id.
     *
     * @param photoId id of photo to rotate
     */
    public void mirrorPhoto(int photoId) {
        connection = Database.getInstance().getConnection();

        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_UPDATE_MIRROR_PHOTO)) {
            try {
                stmt.setInt(1, photoId);
                stmt.setInt(2, photoId);
                stmt.executeUpdate();
            } finally {
                stmt.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets si features to image.
     *
     * @param photoId
     */
    private void updateImageWithSIFeatures(int photoId) {
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_UPDATE_IMAGE_WITH_SI)) {
            try {
                stmt.setInt(1, photoId);
                stmt.executeUpdate();
            } finally {
                stmt.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_UPDATE_IMAGE_WITH_SI_FEATURES)) {
            try {
                stmt.setInt(1, photoId);
                stmt.executeUpdate();
            } finally {
                stmt.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Finds similar photo to actual photo from database. Photo is taken by id. If database has only one photo, it shows alert box which .
     *
     * @param idPhoto id of photo to rotate
     */
    public OrdImage doSelectSimilarPhoto(int idPhoto) {
        connection = Database.getInstance().getConnection();

        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_SELECT_SIMILARITY)) {
            try {
                stmt.setInt(1, idPhoto);
                try (OracleResultSet resultSet = (OracleResultSet) stmt.executeQuery()) {
                    resultSet.next();
                    return (OrdImage) resultSet.getORAData("photo", OrdImage.getORADataFactory());
                } catch (SQLException e) {
                    if (e.getErrorCode() == 17289) {
                        //in database is only one photo
                        AlertBox.display("Only one photo in database",
                                "Cannot show similar photo because there is only this photo in database." +
                                        "\nPlease add more photos to cameras and try again.");
                        return null;
                    }
                } finally {
                    stmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
