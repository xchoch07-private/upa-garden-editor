package cz.vutbr.fit.upa.model;

import cz.vutbr.fit.upa.controller.GardenEditorMainController;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import oracle.spatial.geometry.JGeometry;

import java.util.ArrayList;

/**
 * Class used for convert Database item to correct shape type.
 */
public class ItemConvertAndProperties {

    /**
     * Main controller
     */
    public GardenEditorMainController gardenEditorMainController;

    /**
     * Converts database item and fulfills correct list of class ItemTypeLists with given database item.
     *
     * @param databaseItem item to convert
     * @return ItemTypeLists with lists in it
     */
    public ItemTypeLists convertToShape(DatabaseItem databaseItem) {
        ItemTypeLists itemTypeLists = new ItemTypeLists();

        //circle convert
        if (databaseItem.geometry.getType() == JGeometry.GTYPE_POLYGON && databaseItem.geometry.isCircle()) {
            itemTypeLists.circles = new ArrayList<>();
            ItemCircle itemCircle = new ItemCircle(databaseItem, databaseItem.id);

            double centerY = databaseItem.geometry.getOrdinatesArray()[1] + ((databaseItem.geometry.getOrdinatesArray()[3] - databaseItem.geometry.getOrdinatesArray()[1]) / 2.0);
            double centerX = databaseItem.geometry.getOrdinatesArray()[0];
            itemCircle.setCenterX(centerX);
            itemCircle.setCenterY(centerY);
            itemCircle.setRadius(databaseItem.geometry.getOrdinatesArray()[3] - centerY);

            itemCircle.setFill(Color.BLUE);

            gardenEditorMainController.canvasPaneController.setColorForItem(null, itemCircle, null);

            itemTypeLists.circles.add(itemCircle);

            //polygons convert
        } else if (databaseItem.geometry.getType() == JGeometry.GTYPE_POLYGON && !databaseItem.geometry.isCircle()) {
            itemTypeLists.polygons = new ArrayList<>();
            ItemPolygon itemPolygon = new ItemPolygon(databaseItem, databaseItem.id);

            for (double point : databaseItem.geometry.getOrdinatesArray()) {
                itemPolygon.getPoints().add(point);
            }
            gardenEditorMainController.canvasPaneController.setColorForItem(itemPolygon, null, null);
            itemTypeLists.polygons.add(itemPolygon);
        }

        // point convert
        else if (databaseItem.geometry.getType() == JGeometry.GTYPE_POINT) {
            itemTypeLists.circles = new ArrayList<>();
            ItemCircle itemCircle = new ItemCircle(databaseItem, databaseItem.id);

            itemCircle.setCenterX(databaseItem.geometry.getPoint()[0]);
            itemCircle.setCenterY(databaseItem.geometry.getPoint()[1]);
            itemCircle.setRadius(7.0f);
            gardenEditorMainController.canvasPaneController.setColorForItem(null, itemCircle, null);
            itemTypeLists.circles.add(itemCircle);
        }

        // multipoint convert
        else if (databaseItem.geometry.getType() == JGeometry.GTYPE_MULTIPOINT) {

            itemTypeLists.circles = new ArrayList<>();

            JGeometry[] elements = databaseItem.geometry.getElements();
            for (JGeometry element : elements) {
                ItemCircle itemCircle = new ItemCircle(databaseItem, databaseItem.id);

                itemCircle.setCenterX(element.getPoint()[0]);
                itemCircle.setCenterY(element.getPoint()[1]);
                itemCircle.setRadius(7.0f);
                gardenEditorMainController.canvasPaneController.setColorForItem(null, itemCircle, null);
                itemTypeLists.circles.add(itemCircle);
            }
        }

        // line convert
        else if (databaseItem.geometry.getType() == JGeometry.GTYPE_CURVE) {
            itemTypeLists.paths = new ArrayList<>();
            ItemPath itemPath = new ItemPath(databaseItem, databaseItem.id);

            for (int i = 0; i < databaseItem.geometry.getNumPoints() * 2; i += 2) {

                if (i == 0) {
                    MoveTo moveTo = new MoveTo();
                    moveTo.setX(databaseItem.geometry.getOrdinatesArray()[i]);
                    moveTo.setY(databaseItem.geometry.getOrdinatesArray()[i + 1]);
                    itemPath.getElements().add(moveTo);
                } else {
                    LineTo lineTo = new LineTo();
                    lineTo.setX(databaseItem.geometry.getOrdinatesArray()[i]);
                    lineTo.setY(databaseItem.geometry.getOrdinatesArray()[i + 1]);
                    itemPath.getElements().add(lineTo);
                }
            }
            gardenEditorMainController.canvasPaneController.setColorForItem(null, null, itemPath);
            itemTypeLists.paths.add(itemPath);
        }
        return itemTypeLists;
    }

    /**
     * Injects main controller
     *
     * @param mainController main controller
     */
    public void setMainController(GardenEditorMainController mainController) {
        this.gardenEditorMainController = mainController;
    }

}
