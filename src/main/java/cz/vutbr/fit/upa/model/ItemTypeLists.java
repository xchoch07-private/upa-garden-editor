package cz.vutbr.fit.upa.model;

import java.util.List;

/**
 * Class to keep lists of items of different types.
 */
public class ItemTypeLists {

    /**
     * List of ItemCircles.
     */
    public List<ItemCircle> circles;

    /**
     * List of ItemPolygons.
     */
    public List<ItemPolygon> polygons;

    /**
     * List of ItemPath path.
     */
    public List<ItemPath> paths;
}
