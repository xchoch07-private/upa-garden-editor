package cz.vutbr.fit.upa.model;

import javafx.scene.shape.Polygon;

/**
 * Class representing database item of shape polygon.
 */
public class ItemPolygon extends Polygon {

    /**
     * Reference to database item which represents item of database
     */
    private DatabaseItem itemPolygonReference;

    /**
     * Id of this item
     */
    private int itemIdFromDatabase;

    /**
     * Constructor for creating item polygon.
     *
     * @param itemPolygonReference item reference
     * @param itemIdFromDatabase   id
     */
    public ItemPolygon(DatabaseItem itemPolygonReference, int itemIdFromDatabase) {
        super();
        this.itemPolygonReference = itemPolygonReference;
        this.itemIdFromDatabase = itemIdFromDatabase;
    }

    /**
     * Getter for database item reference
     *
     * @return item reference
     */
    public DatabaseItem getItemPolygonReference() {
        return itemPolygonReference;
    }

    /**
     * Setter for item polygon reference
     *
     * @param itemPolygonReference reference
     */
    public void setItemPolygonReference(DatabaseItem itemPolygonReference) {
        this.itemPolygonReference = itemPolygonReference;
    }

    /**
     * Getter for id.
     *
     * @return id
     */
    public int getItemIdFromDatabase() {
        return itemIdFromDatabase;
    }

    /**
     * Setter for id.
     *
     * @param itemIdFromDatabase
     */
    public void setItemIdFromDatabase(int itemIdFromDatabase) {
        this.itemIdFromDatabase = itemIdFromDatabase;
    }
}
