package cz.vutbr.fit.upa.model;

import cz.vutbr.fit.upa.AlertBox;
import cz.vutbr.fit.upa.Database;
import cz.vutbr.fit.upa.constants.SQLCommands;
import cz.vutbr.fit.upa.controller.GardenEditorMainController;
import cz.vutbr.fit.upa.exception.SqlStatementNotExecuted;
import javafx.scene.shape.Shape;
import oracle.spatial.geometry.JGeometry;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class working with database, inc. save items to database, update and delete items from db
 */
public class ItemDatabaseManipulator {

    /**
     * Main controller
     */
    public GardenEditorMainController gardenEditorMainController;

    /**
     * Connection for connect to database
     */
    private Connection connection;

    /**
     * Add instance of Main controller
     *
     * @param mainController Main controller
     */
    public void setMainController(GardenEditorMainController mainController) {
        this.gardenEditorMainController = mainController;
    }

    /**
     * This method loads all items from database via sql query
     *
     * @return list of items, that saved in database
     * @throws SqlStatementNotExecuted bad syntax of sql query
     */
    public List<DatabaseItem> loadItemsFromDb() throws SqlStatementNotExecuted {
        List<DatabaseItem> itemList = new ArrayList<>();
        connection = Database.getInstance().getConnection();
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_SELECT)) {
            getResultSetValues(itemList, stmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return itemList;
    }

    /**
     * This method gets max id of items from database.
     *
     * @return last id
     */
    public int getLastId() {
        int lastId = 0;
        connection = Database.getInstance().getConnection();
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_GET_MAX_ID)) {
            try (ResultSet resultSet = stmt.executeQuery()) {
                while (resultSet.next()) {
                    lastId = resultSet.getInt("max");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                stmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lastId;
    }

    /**
     * This method saves item to database via sql query
     *
     * @param itemToDB item, that we want save to database
     */
    public void saveItemToDatabaseSQL(DatabaseItem itemToDB) {
        connection = Database.getInstance().getConnection();
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_ADD_ITEM_TO_DB)) {
            try {
                stmt.setInt(1, itemToDB.id);
                stmt.setString(2, itemToDB.name);
                stmt.setString(3, itemToDB.note);
                stmt.setObject(4, JGeometry.storeJS(connection, itemToDB.geometry));
                stmt.setString(5, itemToDB.getItemType());
                stmt.executeUpdate();
            } finally {
                stmt.close();
            }
        } catch (Exception e) {
            AlertBox.display("Select item to save",
                    "Please select button of item you want to add and click to canvas where you want this item.");
        }
    }

    /**
     * This method deletes item from database via sql query
     *
     * @param lastSelectedItem item, that we want delete from database
     */
    public void deleteItemFromDatabaseSQL(Shape lastSelectedItem) throws SQLException {
        int id = getIdLastSelectedItem(lastSelectedItem);
        connection = Database.getInstance().getConnection();

        if (hasPhoto(lastSelectedItem)) {
            //it is needed to delete photo of camera
            connection.setAutoCommit(false);

            ItemPolygon camera = (ItemPolygon) lastSelectedItem;
            PhotoDatabaseManipulator manipulator = new PhotoDatabaseManipulator();
            //need to have connection of this class because of current config
            manipulator.deletePhotoById(connection, camera.getItemPolygonReference().getIdPhoto());

            connection.commit();
            connection.setAutoCommit(true);
        }

        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_DELETE_ITEM_FROM_DB)) {
            try {
                stmt.setInt(1, id);
                stmt.executeUpdate();
            } finally {
                stmt.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method gets id from input item
     *
     * @param lastSelectedItem input item
     * @return id of item
     */
    private int getIdLastSelectedItem(Shape lastSelectedItem) {
        int id = 0;
        if (lastSelectedItem instanceof ItemCircle) {
            ItemCircle circle = (ItemCircle) lastSelectedItem;
            id = circle.getItemIdFromDatabase();
        }

        if (lastSelectedItem instanceof ItemPolygon) {
            ItemPolygon polygon = (ItemPolygon) lastSelectedItem;
            id = polygon.getItemIdFromDatabase();
        }

        if (lastSelectedItem instanceof ItemPath) {
            ItemPath path = (ItemPath) lastSelectedItem;
            id = path.getItemIdFromDatabase();
        }

        return id;
    }

    /**
     * This method updates item in database with new geometry
     *
     * @param id       id of item
     * @param geometry new geometry
     */
    public void updateItemInDatabaseSQL(int id, JGeometry geometry) {
        connection = Database.getInstance().getConnection();
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_UPDATE_ITEM_IN_DB)) {
            try {
                stmt.setInt(2, id);
                stmt.setObject(1, JGeometry.storeJS(connection, geometry));
                stmt.executeUpdate();
            } finally {
                stmt.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method gets area of selected item from database
     *
     * @param id id of item
     * @return area as String
     */
    public String getAreaOfItemSQL(int id) {
        connection = Database.getInstance().getConnection();
        double area = 0.0;
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_GET_AREA_OF_ITEM)) {
            stmt.setInt(1, id);
            try (ResultSet resultSet = stmt.executeQuery()) {
                while (resultSet.next()) {
                    area = resultSet.getDouble("area");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                stmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return String.format("%.2f", area / 100);
    }

    /**
     * This method gets district of selected item from database
     *
     * @param id id of item
     * @return district as String
     */
    public String getDistrictOfItemSQL(int id) {
        connection = Database.getInstance().getConnection();
        double district = 0.0;
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_GET_DISTRICT_OF_ITEM)) {
            stmt.setInt(1, id);
            try (ResultSet resultSet = stmt.executeQuery()) {
                while (resultSet.next()) {
                    district = resultSet.getDouble("district");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                stmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return String.format("%.2f", district / 10);
    }

    /**
     * This method gets all items, that are distant from selected item based on selected distance
     *
     * @param geometry geometry of item
     * @param distance selected distance
     * @return itemList of distant items based on selected distance
     */
    public List<DatabaseItem> getDistanceFromObjectSQL(JGeometry geometry, int distance) {
        distance *= 10;
        String strDistance = String.valueOf(distance);
        List<DatabaseItem> itemList = new ArrayList<>();
        connection = Database.getInstance().getConnection();
        SQLCommands.SQL_GET_DISTANCE_FROM_SELECTED_ITEM = "SELECT * FROM canvas_items i WHERE (SDO_WITHIN_DISTANCE(GEOMETRY, ?, 'distance = " + strDistance + "') = 'TRUE')";

        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_GET_DISTANCE_FROM_SELECTED_ITEM)) {
            stmt.setObject(1, JGeometry.storeJS(connection, geometry));
            getResultSetValues(itemList, stmt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return itemList;
    }

    /**
     * This method fills itemList with items from database
     *
     * @param itemList itemList to fill
     * @param stmt     PreparedStatement
     */
    private void getResultSetValues(List<DatabaseItem> itemList, PreparedStatement stmt) {
        try (ResultSet resultSet = stmt.executeQuery()) {
            while (resultSet.next()) {
                byte[] image = resultSet.getBytes("geometry");
                JGeometry jGeometry = JGeometry.load(image);
                DatabaseItem newItem = new DatabaseItem(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("note"),
                        jGeometry,
                        resultSet.getString("item_type"),
                        resultSet.getInt("id_photo"));
                itemList.add(newItem);
            }
        } catch (Exception e) {
            throw new SqlStatementNotExecuted();
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method gets distance between two items from database
     *
     * @param idOfFirstItem  id of selected item
     * @param idOfSecondItem id of selected item, that selected with button
     * @return distance as String
     */
    public String getDistanceOfTwoItemsSQL(int idOfFirstItem, int idOfSecondItem) {
        connection = Database.getInstance().getConnection();
        double distance = 0.0;
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_GET_DISTANCE_OF_TWO_ITEMS)) {
            stmt.setInt(1, idOfFirstItem);
            stmt.setInt(2, idOfSecondItem);
            try (ResultSet resultSet = stmt.executeQuery()) {
                while (resultSet.next()) {
                    distance = resultSet.getDouble("distance");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                stmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return String.format("%.2f", distance / 10);
    }

    /**
     * This method gets all items, that are covered by Irrigation system
     *
     * @param id id of irrigation system
     * @return list of items, that are covered by Irrigation system
     */
    public List<DatabaseItem> getAllItemsCoveredByIrrigationSystemSQL(int id) {
        List<DatabaseItem> itemList = new ArrayList<>();
        connection = Database.getInstance().getConnection();
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_GET_ALL_ITEMS_COVERED_BY_IRRIGATION_SYSTEM)) {
            stmt.setInt(1, id);
            getResultSetValues(itemList, stmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return itemList;
    }

    /**
     * This method gets nearest items from selected item
     *
     * @param id    id of selected item
     * @param count count of nearest items
     * @return list of nearest items
     */
    public List<DatabaseItem> getNNearestNeighborsSQL(int id, String count) {
        List<DatabaseItem> itemList = new ArrayList<>();
        connection = Database.getInstance().getConnection();
        SQLCommands.SQL_GET_N_NEAREST_NEIGHBORS = "SELECT i2.* FROM canvas_items i1, canvas_items i2 WHERE i1.id = ? AND i1.id <> i2.id AND SDO_NN(i2.geometry, i1.geometry, 'SDO_NUM_RES = " +
                count + "', 1) = 'TRUE' UNION ALL SELECT * FROM canvas_items WHERE id = ?";

        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_GET_N_NEAREST_NEIGHBORS)) {
            stmt.setInt(1, id);
            stmt.setInt(2, id);
            getResultSetValues(itemList, stmt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return itemList;
    }

    /**
     * This method gets note from database of selected item
     *
     * @param id id of selected item
     * @return string as note
     */
    public String getLoadNoteSQL(int id) {
        connection = Database.getInstance().getConnection();
        String note = "";
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_SELECT_NOTE)) {
            stmt.setInt(1, id);
            try (ResultSet resultSet = stmt.executeQuery()) {
                while (resultSet.next()) {
                    note = resultSet.getString("note");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                stmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return note;
    }

    /**
     * This method updates note in database
     *
     * @param id   id of selected item
     * @param note note of selected item
     */
    public void updateNoteSQL(int id, String note) {
        connection = Database.getInstance().getConnection();
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_UPDATE_NOTE_IN_DATABASE)) {
            try {
                stmt.setString(1, note);
                stmt.setInt(2, id);
                stmt.executeQuery();
            } finally {
                stmt.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method deletes all items from table canvas_items
     */
    public void deleteAllFromTableCanvasItemsSQL() {
        connection = Database.getInstance().getConnection();
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_DELETE_ALL_FROM_TABLE_CANVAS_ITEMS)) {
            try {
                stmt.executeQuery();
            } finally {
                stmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method deletes all items from table camera_photos
     */
    public void deleteAllFromTableCameraPhotosSQL() {
        connection = Database.getInstance().getConnection();
        try (PreparedStatement stmt = connection.prepareStatement(SQLCommands.SQL_DELETE_ALL_FROM_TABLE_CAMERA_PHOTOS)) {
            try {
                stmt.executeQuery();
            } finally {
                stmt.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method checks if item is polygon and has some photo
     *
     * @param lastSelectedItem selected item
     * @return true if item has photo, false if error
     */
    private boolean hasPhoto(Shape lastSelectedItem) {
        try {
            return ((ItemPolygon) lastSelectedItem).getItemPolygonReference().getIdPhoto() != 0;
        } catch (ClassCastException ex) {
            return false;
        }
    }
}
