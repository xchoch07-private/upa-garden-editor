package cz.vutbr.fit.upa.model;

/**
 * Model of connection data and user credentials for connecting.
 */
public class ConnectionData {

    /**
     * Host data
     */
    private String host;

    /**
     * Port data
     */
    private int port;

    /**
     * Service name data
     */
    private String serviceName;

    /**
     * Username credential
     */
    private String username;

    /**
     * Password credential
     */
    private String password;

    /**
     * Constructor which creates Connection data.
     *
     * @param host        host
     * @param port        port
     * @param serviceName service name
     * @param username    username
     * @param password    password
     */
    public ConnectionData(String host, int port, String serviceName, String username, String password) {
        this.host = host;
        this.port = port;
        this.serviceName = serviceName;
        this.username = username;
        this.password = password;
    }

    /**
     * Getter for host.
     *
     * @return host
     */
    public String getHost() {
        return host;
    }

    /**
     * Getter for port.
     *
     * @return port
     */
    public int getPort() {
        return port;
    }

    /**
     * Getter for service name.
     *
     * @return service name
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Getter for username.
     *
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Getter for password.
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Returns this object as formatted String.
     *
     * @return
     */
    @Override
    public String toString() {
        return "ConnectionData{" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", serviceName='" + serviceName + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
