package cz.vutbr.fit.upa;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Class which shows new frame. It is used for error and info messages to user.
 */
public class AlertBox {

    /**
     * Width of alert box
     */
    private static int width = 370;

    /**
     * Height of alert box
     */
    private static int height = 150;

    /**
     * Displays new frame with specified title and message.
     *
     * @param title   title of new frame
     * @param message message inside new frame (e.g. info message about connection success)
     */
    public static void display(String title, String message) {
        Stage stage = new Stage();

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle(title);
        stage.setResizable(false);
        stage.setWidth(width);
        stage.setHeight(height);

        Label labelMsg = new Label();
        labelMsg.setText(message);
        labelMsg.setWrapText(true);
        labelMsg.setPadding(new Insets(0, 15, 0, 15));

        Button okButton = new Button("Ok");
        okButton.setOnAction(e -> stage.close());

        VBox layout = new VBox(10);
        layout.setStyle("-fx-background-color: linear-gradient(to bottom right, #888888, #666666); -fx-color: #666666");
        layout.getChildren().addAll(labelMsg, okButton);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        scene.getStylesheets().add("fxml/style.css");
        stage.setScene(scene);
        stage.showAndWait();
    }
}
