package cz.vutbr.fit.upa;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;

/**
 * Main class which starts application.
 */
public class GardenEditor extends Application {

    /**
     * Classpath to image which represents photo of last selected item of canvas.
     */
    public static String IMAGE_CLASSPATH = "./src/main/resources/image.png";

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/GardenEditorMain.fxml"));
        primaryStage.setTitle("Garden Editor");
        Scene scene = new Scene(root, 1230, 750);
        scene.getStylesheets().add("fxml/style.css");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

    }

    @Override
    public void stop() {
        File fileImage = new File(IMAGE_CLASSPATH);
        fileImage.delete();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
