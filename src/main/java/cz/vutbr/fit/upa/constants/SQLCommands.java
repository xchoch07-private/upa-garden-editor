package cz.vutbr.fit.upa.constants;

/**
 * This class represents SQL query constants
 */
public class SQLCommands {

    /**
     * Select all items from table
     */
    public static final String SQL_SELECT = "SELECT * FROM canvas_items";

    /**
     * Select max id from table
     */
    public static final String SQL_GET_MAX_ID = "SELECT MAX(id) AS max FROM canvas_items";

    /**
     * Insert new item to table
     */
    public static final String SQL_ADD_ITEM_TO_DB = "INSERT INTO canvas_items (id, name, note, geometry, item_type) VALUES( ?, ?, ?, ?, ?)";

    /**
     * Delete choose item from table
     */
    public static final String SQL_DELETE_ITEM_FROM_DB = "DELETE FROM canvas_items WHERE id = ?";

    /**
     * Update choose item in table
     */
    public static final String SQL_UPDATE_ITEM_IN_DB = "UPDATE canvas_items SET geometry = ? WHERE id = ?";

    /**
     * Select area of item from sdo_geometry
     */
    public static final String SQL_GET_AREA_OF_ITEM = "SELECT SDO_GEOM.SDO_AREA(canvas_items.geometry) AS area FROM canvas_items WHERE id = ?";

    /**
     * Select district of item from sdo_geometry
     */
    public static final String SQL_GET_DISTRICT_OF_ITEM = "SELECT SDO_GEOM.SDO_LENGTH(canvas_items.geometry) AS district FROM canvas_items WHERE id = ?";

    /**
     * Select all items, that are at specified distance from the selected item
     */
    public static String SQL_GET_DISTANCE_FROM_SELECTED_ITEM = "SELECT * FROM canvas_items i WHERE (SDO_WITHIN_DISTANCE(GEOMETRY, ?, 'distance = ?') = 'TRUE')";

    /**
     * Select distance between two selected items
     */
    public static final String SQL_GET_DISTANCE_OF_TWO_ITEMS = "SELECT SDO_GEOM.SDO_DISTANCE(i1.geometry, i2.geometry) as distance FROM canvas_items i1, canvas_items i2 WHERE i1.id = ? AND i2.id = ?";

    /**
     * Select all items, that are covered by Irrigation system
     */
    public static final String SQL_GET_ALL_ITEMS_COVERED_BY_IRRIGATION_SYSTEM = "SELECT i2.* FROM canvas_items i1, canvas_items i2 WHERE i1.id = ? AND i1.id <> i2.id AND SDO_RELATE(i1.geometry, i2.geometry, 'MASK=contains') = 'TRUE' AND i2.item_type <> 'Camera'";

    /**
     * Select n nearest neighbors by selected item
     */
    public static String SQL_GET_N_NEAREST_NEIGHBORS = "SELECT i2.* FROM canvas_items i1, canvas_items i2 WHERE i1.id = ? AND i1.id <> i2.id AND SDO_NN(i2.geometry, i1.geometry, 'SDO_NUM_RES = ?', 1) = 'TRUE' UNION ALL SELECT * FROM canvas_items WHERE id = ?";

    /**
     * Select note from database
     */
    public static final String SQL_SELECT_NOTE = "SELECT note FROM canvas_items WHERE id = ?";

    /**
     * Update note in database
     */
    public static final String SQL_UPDATE_NOTE_IN_DATABASE = "UPDATE canvas_items SET note = ? WHERE id = ?";

    /**
     * Delete all data from table canvas_items
     */
    public static final String SQL_DELETE_ALL_FROM_TABLE_CANVAS_ITEMS= "DELETE FROM canvas_items";

    /**
     * Delete all data from table camera_photos
     */
    public static final String SQL_DELETE_ALL_FROM_TABLE_CAMERA_PHOTOS = "DELETE FROM camera_photos";

    /**
     * Selects maximal id from photo database
     */
    public static final String SQL_GET_MAX_ID_OF_PHOTO = "SELECT MAX(id_photo) AS max FROM camera_photos";

    /**
     * Sets foreign key id photo of canvas item
     */
    public static final String SQL_UPDATE_ID_PHOTO_OF_CANVAS_ITEM = "UPDATE canvas_items SET id_photo = ? WHERE id = ?";

    /**
     * Sets photo to initialized photo
     */
    public static final String SQL_UPDATE_SET_PHOTO = "UPDATE camera_photos SET photo = ? WHERE id_photo= ?";

    /**
     * Inits new photo to photo database
     */
    public static final String SQL_INIT_NEW_PHOTO =  "INSERT INTO camera_photos(id_photo, photo) values (?, ordsys.ordimage.init())";

    /**
     * Selects photo by id
     */
    public static final String SQL_SELECT_PHOTO_BY_ID =  "SELECT photo FROM camera_photos WHERE id_photo= ? FOR UPDATE";

    /**
     * Selects id photo of camera item
     */
    public static final String SQL_GET_ID_PHOTO_OF_CAMERA = "SELECT id_photo FROM canvas_items WHERE id = ?";

    /**
     * Deletes photo from photo database based on id
     */
    public static final String SQL_DELETE_PHOTO_FROM_DB = "DELETE FROM camera_photos WHERE id_photo = ?";

    /**
     * Updates photo with operation mirror
     */
    public static final String SQL_UPDATE_MIRROR_PHOTO = "DECLARE image ORDSYS.ORDImage;"
            + "BEGIN"
            + "   SELECT photo INTO image FROM camera_photos"
            + "   WHERE id_photo = ? FOR UPDATE;"
            + "   ORDSYS.ORDImage.process(image, 'mirror');"
            + "   image.setProperties;"
            + "   UPDATE camera_photos SET photo = image WHERE id_photo = ?;"
            + "END;";

    /**
     * Updates photo with operation rotate right
     */
    public static final String SQL_UPDATE_ROTATE_RIGHT_PHOTO = "DECLARE image ORDSYS.ORDImage;"
            + "BEGIN"
            + "   SELECT photo INTO image FROM camera_photos"
            + "   WHERE id_photo = ? FOR UPDATE;"
            + "   ORDSYS.ORDImage.process(image, 'rotate = 90');"
            + "   image.setProperties;"
            + "   UPDATE camera_photos SET photo = image WHERE id_photo = ?;"
            + "END;";

    /**
     * Updates photo with operation rotate left
     */
    public static final String SQL_UPDATE_ROTATE_LEFT_PHOTO = "DECLARE image ORDSYS.ORDImage;"
            + "BEGIN"
            + "   SELECT photo INTO image FROM camera_photos"
            + "   WHERE id_photo = ? FOR UPDATE;"
            + "   ORDSYS.ORDImage.process(image, 'rotate = -90');"
            + "   image.setProperties;"
            + "   UPDATE camera_photos SET photo = image WHERE id_photo = ?;"
            + "END;";

    /**
     * Updates photo with operation rotate upside down (rotate twice)
     */
    public static final String SQL_UPDATE_ROTATE_TWICE_PHOTO = "DECLARE image ORDSYS.ORDImage;"
            + "BEGIN"
            + "   SELECT photo INTO image FROM camera_photos"
            + "   WHERE id_photo = ? FOR UPDATE;"
            + "   ORDSYS.ORDImage.process(image, 'rotate = 180');"
            + "   image.setProperties;"
            + "   UPDATE camera_photos SET photo = image WHERE id_photo = ?;"
            + "END;";

    /**
     * Updates photo with still image needed for similarity operation.
     */
    public static final String SQL_UPDATE_IMAGE_WITH_SI = "UPDATE camera_photos p set" +
            " p.photo_si = SI_StillImage(p.photo.getContent()) WHERE id_photo = ?";

    /**
     * Updates photo with still image properties needed for similarity operation.
     */
    public static final String SQL_UPDATE_IMAGE_WITH_SI_FEATURES = "UPDATE camera_photos p set " +
            "p.photo_ac = SI_AverageColor(p.photo_si)," +
            "p.photo_ch = SI_ColorHistogram(p.photo_si)," +
            "p.photo_pc = SI_PositionalColor(p.photo_si)," +
            "p.photo_tx = SI_Texture(p.photo_si) WHERE id_photo = ?";

    /**
     * Selects photo with similar properties such as average color.
     */
    public static final String SQL_SELECT_SIMILARITY = "SELECT dst.*," +
            " SI_ScoreByFtrList(new SI_FeatureList(src.photo_ac, 0.5, src.photo_ch, 0.2, src.photo_pc , 0.1, src.photo_tx, 0.2), dst.photo_si) as similarity " +
            "FROM camera_photos src , camera_photos dst " +
            "WHERE (src.id_photo <> dst.id_photo) AND src.id_photo = ? ORDER BY similarity ASC";

}

