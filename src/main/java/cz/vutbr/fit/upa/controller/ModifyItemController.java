package cz.vutbr.fit.upa.controller;

import cz.vutbr.fit.upa.model.ItemCircle;
import cz.vutbr.fit.upa.model.ItemPath;
import cz.vutbr.fit.upa.model.ItemPolygon;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.InputEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.shape.Shape;
import oracle.spatial.geometry.JGeometry;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller of ModifyItemPane.fxml
 */
public class ModifyItemController implements Initializable {

    /**
     * Main controller
     */
    public GardenEditorMainController gardenEditorMainController;

    /**
     * Toggle group for modify items
     */
    @FXML
    public ToggleGroup toggleButtonsModify;

    /**
     * Delete button for items
     */
    @FXML
    public Button deleteButtonId;

    /**
     * Note value in Accordion menu
     */
    @FXML
    public TextField noteValue;

    /**
     * Value of modify buttons
     */
    public String modifyItemButtonSelect = "";

    /**
     * Starting x coordinate of mouse clicked
     */
    private double startX = 0.0;

    /**
     * Starting y coordinate of mouse clicked
     */
    private double startY = 0.0;

    /**
     * Auxiliary attribute for work with Item polygon
     */
    private ItemPolygon itemGeometryPolygon = null;

    /**
     * Auxiliary attribute for work with Item circle
     */
    private ItemCircle itemGeometryCircle = null;

    /**
     * Auxiliary attribute for work with Item path
     */
    private ItemPath itemGeometryPath = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        getValueFromSelectedToggleButtonModify();
    }

    /**
     * Handles Toggle buttons from Toggle group
     * Gets button value if selected
     * Sets default value if not selected
     */
    public void getValueFromSelectedToggleButtonModify() {
        toggleButtonsModify.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                deleteButtonId.setDisable(false);
                modifyItemButtonSelect = "Nothing";
            } else {
                ToggleButton tb = (ToggleButton) newValue.getToggleGroup().getSelectedToggle();
                if (tb.isSelected()) {
                    modifyItemButtonSelect = tb.getText();
                    deleteButtonId.setDisable(true);
                }
            }
        });
    }

    /**
     * Gets instance of Main controller
     *
     * @param mainController - instance of GardenEditorMainController
     */
    public void setMainController(GardenEditorMainController mainController) {
        this.gardenEditorMainController = mainController;
    }

    /**
     * Decides which modify operation to perform and calls relevant method
     *
     * @param event event
     */
    public void modifyItem(InputEvent event) {
        switch (modifyItemButtonSelect) {
            case "Resize":
                resizeItem(event);
                break;
            case "Move":
                moveItem(event);
                break;
            case "Rotate":
                rotateItem(event);
                break;
            default:
                break;
        }
    }

    /**
     * Checks instance of item and updates actual item in database
     */
    private void updateToDB() {
        if (gardenEditorMainController.canvasPaneController.lastSelectedItem != null) {
            if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPolygon) {
                gardenEditorMainController.itemDatabaseManipulator.updateItemInDatabaseSQL(itemGeometryPolygon.getItemPolygonReference().id, itemGeometryPolygon.getItemPolygonReference().geometry);
            } else if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemCircle) {
                gardenEditorMainController.itemDatabaseManipulator.updateItemInDatabaseSQL(itemGeometryCircle.getItemCircleReference().id, itemGeometryCircle.getItemCircleReference().geometry);
            } else if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPath) {
                gardenEditorMainController.itemDatabaseManipulator.updateItemInDatabaseSQL(itemGeometryPath.getItemPathReference().id, itemGeometryPath.getItemPathReference().geometry);
            }
        }
    }

    /**
     * Controls border while item is moving, resizing or rotating
     *
     * @param item geometry of actual editing item
     * @return false if item inside of border, true if item outside of border
     */
    public boolean controlBorder(JGeometry item) {
        JGeometry canvas = new JGeometry(3, 0, new int[]{1, 1003, 1},
                new double[]{0, 0, 840, 0, 840, 720, 0, 720, 0, 0}
        );

        try {
            return !item.isInside(canvas, 0, "FALSE");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * This method is for resizing item. Method first knows which, what instance of the item it's and edits geometry by mouse scroll.
     * Then calls method for redraw canvas and saves updated geometry to database.
     *
     * @param event mouse event scroll
     */
    private void resizeItem(InputEvent event) {
        if (gardenEditorMainController.canvasPaneController.lastSelectedItem != null) {
            if (event.getEventType() == ScrollEvent.SCROLL) {
                double scrollValue = ((ScrollEvent) event).getDeltaY();
                JGeometry geometry;

                if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPolygon) {
                    itemGeometryPolygon = (ItemPolygon) gardenEditorMainController.canvasPaneController.lastSelectedItem;
                    if (!itemGeometryPolygon.getItemPolygonReference().getItemType().equals("Camera")) {
                        JGeometry oldItemGeometry = itemGeometryPolygon.getItemPolygonReference().geometry;
                        geometry = itemGeometryPolygon.getItemPolygonReference().geometry;
                        JGeometry jGeometryForAffineTransform = new JGeometry(geometry.getFirstPoint()[0], geometry.getFirstPoint()[1], 0);

                        try {
                            itemGeometryPolygon.getItemPolygonReference().geometry = geometry.affineTransforms(true, 0, 0,
                                    0, true, jGeometryForAffineTransform, 1 + (scrollValue / 1200), (1 + scrollValue / 1200), 0, false, null, null, 0, 0, false, 0, 0, 0, 0, 0, 0, false, null, null, 0, false, null, null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (controlBorder(itemGeometryPolygon.getItemPolygonReference().geometry)) {

                            itemGeometryPolygon.getItemPolygonReference().geometry = oldItemGeometry;
                        }
                        gardenEditorMainController.canvasPaneController.draw();
                    }
                } else if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemCircle) {
                    itemGeometryCircle = (ItemCircle) gardenEditorMainController.canvasPaneController.lastSelectedItem;
                    JGeometry oldItemGeometry = itemGeometryCircle.getItemCircleReference().geometry;
                    geometry = itemGeometryCircle.getItemCircleReference().geometry;

                    JGeometry jGeometryForAffineTransform = new JGeometry(geometry.getFirstPoint()[0], geometry.getFirstPoint()[1], 0);
                    try {
                        itemGeometryCircle.getItemCircleReference().geometry = geometry.affineTransforms(true, 0, 0,
                                0, true, jGeometryForAffineTransform, 1 + (scrollValue / 800), (1 + scrollValue / 800), 0, false, null, null, 0, 0, false, 0, 0, 0, 0, 0, 0, false, null, null, 0, false, null, null);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (controlBorder(itemGeometryCircle.getItemCircleReference().geometry)) {
                        itemGeometryCircle.getItemCircleReference().geometry = oldItemGeometry;
                    }

                    gardenEditorMainController.canvasPaneController.draw();


                } else if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPath) {
                    itemGeometryPath = (ItemPath) gardenEditorMainController.canvasPaneController.lastSelectedItem;
                    JGeometry oldItemGeometry = itemGeometryPath.getItemPathReference().geometry;
                    geometry = itemGeometryPath.getItemPathReference().geometry;
                    JGeometry jGeometryForAffineTransform = new JGeometry(geometry.getFirstPoint()[0], geometry.getFirstPoint()[1], 0);

                    try {
                        itemGeometryPath.getItemPathReference().geometry = geometry.affineTransforms(true, 0, 0,
                                0, true, jGeometryForAffineTransform, 1 + (scrollValue / 800), (1 + scrollValue / 800), 0, false, null, null, 0, 0, false, 0, 0, 0, 0, 0, 0, false, null, null, 0, false, null, null);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (controlBorder(itemGeometryPath.getItemPathReference().geometry)) {
                        itemGeometryPath.getItemPathReference().geometry = oldItemGeometry;
                    }

                    gardenEditorMainController.canvasPaneController.draw();

                }
                updateToDB();
            }
        }
    }


    /**
     * This method is for moving item. Method first knows which, what instance of the item it's and edits geometry by mouse move.
     * Then calls method for redraw canvas and saves updated geometry database.
     *
     * @param event mouse event drag and drop
     */
    private void moveItem(InputEvent event) {
        if (event.getEventType() == ScrollEvent.SCROLL) {
            return;
        }
        MouseEvent mouseEvent = (MouseEvent) event;
        JGeometry oldGeometry;

        if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
            startX = mouseEvent.getX();
            startY = mouseEvent.getY();
        }

        if (gardenEditorMainController.canvasPaneController.lastSelectedItem != null) {
            if (event.getEventType() == MouseEvent.MOUSE_DRAGGED) {
                if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPolygon) {
                    itemGeometryPolygon = (ItemPolygon) gardenEditorMainController.canvasPaneController.lastSelectedItem;
                    JGeometry oldItemGeometry = itemGeometryPolygon.getItemPolygonReference().geometry;
                    oldGeometry = itemGeometryPolygon.getItemPolygonReference().geometry;
                    try {
                        itemGeometryPolygon.getItemPolygonReference().geometry = oldGeometry.affineTransforms(true, mouseEvent.getX() - startX, mouseEvent.getY() - startY,
                                0, false, null, 0, 0, 0, false, null, null, 0, 0, false, 0, 0, 0, 0, 0, 0, false, null, null, 0, false, null, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (controlBorder(itemGeometryPolygon.getItemPolygonReference().geometry)) {
                        itemGeometryPolygon.getItemPolygonReference().geometry = oldItemGeometry;
                    }

                    gardenEditorMainController.canvasPaneController.draw();
                } else if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemCircle) {
                    itemGeometryCircle = (ItemCircle) gardenEditorMainController.canvasPaneController.lastSelectedItem;
                    JGeometry oldItemGeometry = itemGeometryCircle.getItemCircleReference().geometry;

                    oldGeometry = itemGeometryCircle.getItemCircleReference().geometry;

                    try {
                        itemGeometryCircle.getItemCircleReference().geometry = oldGeometry.affineTransforms(true, mouseEvent.getX() - startX, mouseEvent.getY() - startY,
                                0, false, null, 0, 0, 0, false, null, null, 0, 0, false, 0, 0, 0, 0, 0, 0, false, null, null, 0, false, null, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (controlBorder(itemGeometryCircle.getItemCircleReference().geometry)) {
                        itemGeometryCircle.getItemCircleReference().geometry = oldItemGeometry;
                    }

                    gardenEditorMainController.canvasPaneController.draw();
                } else if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPath) {
                    itemGeometryPath = (ItemPath) gardenEditorMainController.canvasPaneController.lastSelectedItem;
                    JGeometry oldItemGeometry = itemGeometryPath.getItemPathReference().geometry;
                    oldGeometry = itemGeometryPath.getItemPathReference().geometry;

                    try {
                        itemGeometryPath.getItemPathReference().geometry = oldGeometry.affineTransforms(true, mouseEvent.getX() - startX, mouseEvent.getY() - startY,
                                0, false, null, 0, 0, 0, false, null, null, 0, 0, false, 0, 0, 0, 0, 0, 0, false, null, null, 0, false, null, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (controlBorder(itemGeometryPath.getItemPathReference().geometry)) {
                        itemGeometryPath.getItemPathReference().geometry = oldItemGeometry;
                    }

                    gardenEditorMainController.canvasPaneController.draw();
                }
                startX = mouseEvent.getX();
                startY = mouseEvent.getY();
            } else if (event.getEventType() == MouseEvent.MOUSE_RELEASED) {
                updateToDB();
            }
        }
    }

    /**
     * This method is for rotate item. Method first knows which, what instance of the item it's and edits geometry by mouse scroll.
     * Then calls method for redraw canvas and saves updated geometry database.
     *
     * @param event mouse event drag and drop
     */
    private void rotateItem(InputEvent event) {
        if (gardenEditorMainController.canvasPaneController.lastSelectedItem != null) {
            if (event.getEventType() == ScrollEvent.SCROLL) {
                double scrollValue = ((ScrollEvent) event).getDeltaY();
                JGeometry oldGeometry;

                if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPolygon) {
                    itemGeometryPolygon = (ItemPolygon) gardenEditorMainController.canvasPaneController.lastSelectedItem;
                    if (!itemGeometryPolygon.getItemPolygonReference().getItemType().equals("Camera")) {
                        JGeometry oldItemGeometry = itemGeometryPolygon.getItemPolygonReference().geometry;
                        oldGeometry = itemGeometryPolygon.getItemPolygonReference().geometry;
                        JGeometry jGeometryForAffineTransform = new JGeometry(oldGeometry.getFirstPoint()[0], oldGeometry.getFirstPoint()[1], 0);

                        try {
                            itemGeometryPolygon.getItemPolygonReference().geometry = oldGeometry.affineTransforms(false, 0, 0, 0,
                                    false, null, 0, 0, 0,
                                    true, jGeometryForAffineTransform, null, 1 * (scrollValue / 800), -1, false, 0, 0, 0, 0, 0, 0, false, null, null, 0, false, null, null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (controlBorder(itemGeometryPolygon.getItemPolygonReference().geometry)) {
                            itemGeometryPolygon.getItemPolygonReference().geometry = oldItemGeometry;
                        }

                        gardenEditorMainController.canvasPaneController.draw();
                        updateToDB();
                    }

                } else if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPath) {
                    itemGeometryPath = (ItemPath) gardenEditorMainController.canvasPaneController.lastSelectedItem;
                    JGeometry oldItemGeometry = itemGeometryPath.getItemPathReference().geometry;
                    oldGeometry = itemGeometryPath.getItemPathReference().geometry;
                    JGeometry jGeometryForAffineTransform = new JGeometry(oldGeometry.getFirstPoint()[0], oldGeometry.getFirstPoint()[1], 0);

                    try {
                        itemGeometryPath.getItemPathReference().geometry = oldGeometry.affineTransforms(false, 0, 0, 0,
                                false, null, 0, 0, 0,
                                true, jGeometryForAffineTransform, null, 1 * (scrollValue / 800), -1, false, 0, 0, 0, 0, 0, 0, false, null, null, 0, false, null, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (controlBorder(itemGeometryPath.getItemPathReference().geometry)) {
                        itemGeometryPath.getItemPathReference().geometry = oldItemGeometry;
                    }

                    gardenEditorMainController.canvasPaneController.draw();
                    updateToDB();
                }

            }
        }
    }

    /**
     * Deletes item from canvas and calls method for deleting item from database
     */
    @FXML
    public void deleteItem() throws SQLException {
        //reason of list - handling delete of multipoint as one action
        List<Shape> itemsForDelete = new ArrayList<>();
        for (Node node : gardenEditorMainController.canvasPaneController.canvas.getChildren()) {
            Shape canvasItem = (Shape) node;

            Shape item = gardenEditorMainController.canvasPaneController.lastSelectedItem;
            if ((canvasItem instanceof ItemCircle) && (item instanceof ItemCircle)) {
                ItemCircle selectedCircle = (ItemCircle) canvasItem;

                if (selectedCircle.getItemIdFromDatabase() == ((ItemCircle) item).getItemIdFromDatabase()) {
                    gardenEditorMainController.itemDatabaseManipulator.deleteItemFromDatabaseSQL(item);
                    itemsForDelete.add(selectedCircle);
                }

            } else if ((canvasItem instanceof ItemPolygon) && (item instanceof ItemPolygon)) {

                ItemPolygon selectedPolygon = (ItemPolygon) canvasItem;

                if (selectedPolygon.getItemIdFromDatabase() == ((ItemPolygon) item).getItemIdFromDatabase()) {
                    gardenEditorMainController.itemDatabaseManipulator.deleteItemFromDatabaseSQL(item);
                    itemsForDelete.add(selectedPolygon);
                }
            } else if ((canvasItem instanceof ItemPath) && (item instanceof ItemPath)) {
                ItemPath selectedPath = (ItemPath) canvasItem;

                if (selectedPath.getItemIdFromDatabase() == ((ItemPath) item).getItemIdFromDatabase()) {
                    gardenEditorMainController.itemDatabaseManipulator.deleteItemFromDatabaseSQL(item);
                    itemsForDelete.add(selectedPath);
                }
            }
        }

        for (Shape item : itemsForDelete) {
            gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(item);
        }
        gardenEditorMainController.canvasPaneController.callLoadItemsFromDb();
        gardenEditorMainController.canvasPaneController.lastSelectedItem = null;
        gardenEditorMainController.photosOfCameraPaneController.showCameraNotChosenPane();
    }

    /**
     * Loads note value to TextField in Accordion menu
     *
     * @param id id of selected item
     */
    public void loadNote(int id) {
        noteValue.setText(gardenEditorMainController.itemDatabaseManipulator.getLoadNoteSQL(id));
    }

    /**
     * Updates note value in Accordion menu and saves it to database
     */
    @FXML
    public void updateNote() {
        if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPolygon) {
            ItemPolygon polygon = (ItemPolygon) gardenEditorMainController.canvasPaneController.lastSelectedItem;
            gardenEditorMainController.itemDatabaseManipulator.updateNoteSQL(polygon.getItemPolygonReference().id, noteValue.getText());
        }

        if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemCircle) {
            ItemCircle circle = (ItemCircle) gardenEditorMainController.canvasPaneController.lastSelectedItem;
            gardenEditorMainController.itemDatabaseManipulator.updateNoteSQL(circle.getItemCircleReference().id, noteValue.getText());
        }

        if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPath) {
            ItemPath path = (ItemPath) gardenEditorMainController.canvasPaneController.lastSelectedItem;
            gardenEditorMainController.itemDatabaseManipulator.updateNoteSQL(path.getItemPathReference().id, noteValue.getText());
        }
        noteValue.setText("");
    }
}
