package cz.vutbr.fit.upa.controller;

import cz.vutbr.fit.upa.exception.SqlStatementNotExecuted;
import cz.vutbr.fit.upa.model.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.input.InputEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import oracle.ord.im.OrdImage;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller of Map.fxml
 */
public class CanvasController implements Initializable {

    /**
     * Canvas from fxml
     */
    @FXML
    public AnchorPane canvas;

    /**
     * List of all items in canvas
     */
    List<DatabaseItem> itemList;

    /**
     * Actual select shape in canvas
     */
    Shape lastSelectedItem = null;

    /**
     * Actual second select shape in canvas for distance operation
     */
    Shape lastSelectItemForDistance = null;

    /**
     * Instance of {@link GardenEditorMainController}
     */
    public GardenEditorMainController gardenEditorMainController;

    /**
     * Instance of {@link ItemDatabaseManipulator}
     */
    ItemDatabaseManipulator itemDatabaseManipulator = new ItemDatabaseManipulator();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        canvas.addEventHandler(InputEvent.ANY, t -> gardenEditorMainController.selectCanvasHandlerType(t));
    }

    /**
     * Injects main controller
     *
     * @param mainController main controller
     */
    public void setMainController(GardenEditorMainController mainController) {
        this.gardenEditorMainController = mainController;
    }

    /**
     * Loads items from database to list
     *
     * @throws SqlStatementNotExecuted query may not be successful
     */
    public void callLoadItemsFromDb() throws SqlStatementNotExecuted {
        itemList = itemDatabaseManipulator.loadItemsFromDb();
    }

    /**
     * Method draws items to canvas
     */
    public void draw() {
        clearMap();
        for (DatabaseItem databaseItem : itemList) {

            //draw circles
            if (gardenEditorMainController.itemConvertAndProperties.convertToShape(databaseItem).circles != null) {
                for (ItemCircle circle : gardenEditorMainController.itemConvertAndProperties.convertToShape(databaseItem).circles) {
                    canvas.getChildren().add(circle);
                    this.addHandleToObjects(circle);
                }
            }

            //draw polygons
            if (gardenEditorMainController.itemConvertAndProperties.convertToShape(databaseItem).polygons != null) {
                for (ItemPolygon polygon : gardenEditorMainController.itemConvertAndProperties.convertToShape(databaseItem).polygons) {
                    canvas.getChildren().add(polygon);
                    this.addHandleToObjects(polygon);
                }
            }
            //draw lines
            if (gardenEditorMainController.itemConvertAndProperties.convertToShape(databaseItem).paths != null) {
                for (ItemPath line : gardenEditorMainController.itemConvertAndProperties.convertToShape(databaseItem).paths) {

                    if (line.getItemPathReference().getItemType().equals("Main path")) {
                        line.setStrokeWidth(45.0);
                    } else if (line.getItemPathReference().getItemType().equals("Path")) {
                        line.setStrokeWidth(15.0);
                    }
                    canvas.getChildren().add(line);
                    this.addHandleToObjects(line);
                }
            }
        }
    }

    /**
     * Method sets original color to lastSelected item when new is select
     *
     * @param SelectedItem selected item
     */
    public void setOriginalColor(Shape SelectedItem) {
        for (Node node : canvas.getChildren()) {
            Shape canvasItem = (Shape) node;

            if ((SelectedItem instanceof ItemCircle) && (canvasItem instanceof ItemCircle)) {
                ItemCircle selectedCircle = (ItemCircle) canvasItem;
                if (selectedCircle.getItemIdFromDatabase() == ((ItemCircle) SelectedItem).getItemIdFromDatabase()) {
                    setColorForItem(null, selectedCircle, null);
                }
            } else if ((SelectedItem instanceof ItemPolygon) && (canvasItem instanceof ItemPolygon)) {
                ItemPolygon selectedPolygon = (ItemPolygon) canvasItem;
                if (selectedPolygon.getItemIdFromDatabase() == ((ItemPolygon) SelectedItem).getItemIdFromDatabase()) {
                    setColorForItem(selectedPolygon, null, null);
                }
            } else if ((SelectedItem instanceof ItemPath) && (canvasItem instanceof ItemPath)) {
                ItemPath selectedPath = (ItemPath) canvasItem;
                if (selectedPath.getItemIdFromDatabase() == ((ItemPath) SelectedItem).getItemIdFromDatabase()) {
                    setColorForItem(null, null, selectedPath);
                }
            }
        }
    }

    /**
     * Adds different items handler for all menu type
     *
     * @param item item from canvas
     */
    public void addHandleToObjects(Shape item) {
        item.addEventHandler(InputEvent.ANY, t -> {
            if (t.getEventType() == MouseEvent.MOUSE_PRESSED) {

                if (!gardenEditorMainController.selectedAccordionMenu.equals("Add Item")) {
                    if (gardenEditorMainController.selectsPaneController.secondItemButton.isSelected()) {
                        if (lastSelectItemForDistance != null) {
                            setOriginalColor(lastSelectItemForDistance);
                        }

                        lastSelectItemForDistance = item;

                        showCorrectPhotoPane(lastSelectedItem);

                        gardenEditorMainController.selectsPaneController.loadGeometryOfItem(lastSelectedItem);

                        for (Node node : canvas.getChildren()) {
                            Shape canvasItem = (Shape) node;
                            if ((canvasItem instanceof ItemCircle) && (item instanceof ItemCircle)) {
                                ItemCircle selectedCircle = (ItemCircle) canvasItem;

                                if (selectedCircle.getItemIdFromDatabase() == ((ItemCircle) item).getItemIdFromDatabase()) {
                                    selectedCircle.setFill(Color.valueOf("orange"));
                                    selectedCircle.setOpacity(1);
                                }

                            } else if ((canvasItem instanceof ItemPolygon) && (item instanceof ItemPolygon)) {

                                ItemPolygon selectedPolygon = (ItemPolygon) canvasItem;

                                if (selectedPolygon.getItemIdFromDatabase() == ((ItemPolygon) item).getItemIdFromDatabase()) {
                                    selectedPolygon.setFill(Color.valueOf("orange"));
                                    selectedPolygon.setOpacity(1);
                                }
                            } else if ((canvasItem instanceof ItemPath) && (item instanceof ItemPath)) {
                                ItemPath selectedPath = (ItemPath) canvasItem;

                                if (selectedPath.getItemIdFromDatabase() == ((ItemPath) item).getItemIdFromDatabase()) {
                                    selectedPath.setStroke(Color.valueOf("orange"));
                                    selectedPath.setOpacity(1);
                                }
                            }
                        }

                        gardenEditorMainController.selectsPaneController.getDistanceOfTwoItems(lastSelectedItem, lastSelectItemForDistance);

                    } else {
                        if (lastSelectedItem != null) {
                            setOriginalColor(lastSelectedItem);
                        }

                        lastSelectedItem = item;

                        showCorrectPhotoPane(lastSelectedItem);

                        gardenEditorMainController.selectsPaneController.loadGeometryOfItem(lastSelectedItem);

                        for (Node node : canvas.getChildren()) {
                            Shape canvasItem = (Shape) node;
                            if ((canvasItem instanceof ItemCircle) && (item instanceof ItemCircle)) {
                                ItemCircle selectedCircle = (ItemCircle) canvasItem;

                                if (selectedCircle.getItemIdFromDatabase() == ((ItemCircle) item).getItemIdFromDatabase()) {
                                    selectedCircle.setFill(Color.valueOf("red"));
                                    selectedCircle.setOpacity(1);
                                    gardenEditorMainController.modifyItemPaneController.loadNote(selectedCircle.getItemCircleReference().id);
                                }

                            } else if ((canvasItem instanceof ItemPolygon) && (item instanceof ItemPolygon)) {

                                ItemPolygon selectedPolygon = (ItemPolygon) canvasItem;

                                if (selectedPolygon.getItemIdFromDatabase() == ((ItemPolygon) item).getItemIdFromDatabase()) {
                                    selectedPolygon.setFill(Color.valueOf("red"));
                                    selectedPolygon.setOpacity(1);
                                    gardenEditorMainController.modifyItemPaneController.loadNote(selectedPolygon.getItemPolygonReference().id);
                                }
                            } else if ((canvasItem instanceof ItemPath) && (item instanceof ItemPath)) {
                                ItemPath selectedPath = (ItemPath) canvasItem;

                                if (selectedPath.getItemIdFromDatabase() == ((ItemPath) item).getItemIdFromDatabase()) {
                                    selectedPath.setStroke(Color.valueOf("red"));
                                    selectedPath.setOpacity(1);
                                    gardenEditorMainController.modifyItemPaneController.loadNote(selectedPath.getItemPathReference().id);
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    /**
     * Sets color for item by item type
     *
     * @param itemPolygon item type is polygon
     * @param itemCircle  item type is circle
     * @param itemPath    item type is path
     */
    public void setColorForItem(ItemPolygon itemPolygon, ItemCircle itemCircle, ItemPath itemPath) {
        String itemType = "";
        if (itemPolygon != null) {
            itemType = itemPolygon.getItemPolygonReference().getItemType();
        } else if (itemCircle != null) {
            itemType = itemCircle.getItemCircleReference().getItemType();
        } else if (itemPath != null) {
            itemType = itemPath.getItemPathReference().getItemType();
        }

        switch (itemType) {
            case "Flower bed":
                itemPolygon.setFill(Color.rgb(96, 64, 32));
                break;
            case "Camera":
                itemPolygon.setFill(Color.rgb(0, 0, 0));
                break;
            case "Irrigation system":
                itemCircle.setFill(Color.rgb(0, 0, 255));
                itemCircle.setOpacity(0.3);
                break;
            case "Path":
                itemPath.setStroke(Color.rgb(204, 153, 102));
                break;
            case "Building":
                itemPolygon.setFill(Color.rgb(166, 166, 166));
                break;
            case "Main path":
                itemPath.setStroke(Color.rgb(89, 89, 89));
                break;
            case "Tree":
                itemCircle.setFill(Color.rgb(0, 85, 0));
                break;
            case "Bush":
                itemCircle.setFill(Color.rgb(0, 170, 0));
                break;
            case "Lake":
                itemCircle.setFill(Color.rgb(0, 160, 160));
                break;
            case "Green house":
                itemPolygon.setFill(Color.rgb(128, 179, 255));
                break;
        }
    }

    /**
     * Deletes all items from canvas
     */
    public void clearMap() {
        canvas.getChildren().clear();
    }

    /**
     * Sets correct photo-pane based on chosen item.
     * Also shows operations of pane with photo and saves this photo.
     * @param lastSelectedItem selected item  on canvas
     */
    private void showCorrectPhotoPane(Shape lastSelectedItem) {
        PhotoDatabaseManipulator photoManipulator = new PhotoDatabaseManipulator();

        if (lastSelectedItem instanceof ItemPolygon) {
            ItemPolygon selectedPolygon = (ItemPolygon) lastSelectedItem;
            if (selectedPolygon.getItemPolygonReference().getItemType().equals("Camera")) {
                gardenEditorMainController.photosOfCameraPaneController.showCameraChosenPane();
                int idPhoto = selectedPolygon.getItemPolygonReference().getIdPhoto();
                if (idPhoto == 0) {
                    gardenEditorMainController.photosOfCameraPaneController.setPhotoOnPane(true);
                    gardenEditorMainController.photosOfCameraPaneController.setOperationsPaneVisibility(false);
                } else {
                    OrdImage ordImage = photoManipulator.doSelectPhotoByIdQuery(idPhoto);
                    gardenEditorMainController.photosOfCameraPaneController.saveAndShowPhoto(ordImage);
                    gardenEditorMainController.photosOfCameraPaneController.setOperationsPaneVisibility(true);

                }
            } else {
                gardenEditorMainController.photosOfCameraPaneController.showCameraNotChosenPane();
            }
        } else {
            gardenEditorMainController.photosOfCameraPaneController.showCameraNotChosenPane();
        }
    }
}
