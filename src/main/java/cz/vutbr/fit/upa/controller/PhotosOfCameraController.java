package cz.vutbr.fit.upa.controller;

import cz.vutbr.fit.upa.AlertBox;
import cz.vutbr.fit.upa.Database;
import cz.vutbr.fit.upa.PhotoAlertBox;
import cz.vutbr.fit.upa.exception.FileNotPhotoException;
import cz.vutbr.fit.upa.model.ItemPolygon;
import cz.vutbr.fit.upa.model.PhotoDatabaseManipulator;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import oracle.ord.im.OrdImage;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

/**
 * Controller of PhotosOCameraPane.fxml.
 */
public class PhotosOfCameraController implements Initializable {

    /**
     * Pane with all operations
     */
    @FXML
    private AnchorPane operationsPane;

    /**
     * Pane which is shown in Photos of camera when camera is chosen by user
     */
    @FXML
    private AnchorPane cameraChosenPane;

    /**
     * Pane which is shown in Photos of camera when camera is not chosen by user
     */
    @FXML
    private AnchorPane cameraNotChosenPane;

    /**
     * Main pane of photos including all
     */
    @FXML
    private AnchorPane photoPane;

    /**
     * Main controller
     */
    public GardenEditorMainController gardenEditorMainController;

    /**
     * Manipulator with photo database
     */
    private PhotoDatabaseManipulator photosManipulator = new PhotoDatabaseManipulator();

    /**
     * Classpath to image, removed on app exit
     */
    private static String IMAGE_CLASSPATH = "./src/main/resources/image.png";

    /**
     * Classpath to similar image, removed on app exit
     */
    private static String SIMILAR_IMAGE_CLASSPATH = "./src/main/resources/similarimage.png";

    /**
     * Classpath to placeholder image
     */
    private static String PLACEHOLDER_CLASSPATH = "./src/main/resources/placeholder.png";

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        showCameraNotChosenPane();
        setOperationsPaneVisibility(false);
        File fileImage = new File(IMAGE_CLASSPATH);
        fileImage.deleteOnExit();
    }

    /**
     * Injects main controller
     *
     * @param mainController main controller
     */
    public void setMainController(GardenEditorMainController mainController) {
        this.gardenEditorMainController = mainController;
    }

    /**
     * Sets operations pane visibility.
     *
     * @param visibility value of visibility to set
     */
    public void setOperationsPaneVisibility(boolean visibility) {
        operationsPane.setVisible(visibility);
    }

    /**
     * Sets camera chosen pane visible.
     */
    public void showCameraChosenPane() {
        cameraChosenPane.setVisible(true);
        cameraNotChosenPane.setVisible(false);
    }

    /**
     * Sets camera not chosen pane visible.
     */
    public void showCameraNotChosenPane() {
        cameraChosenPane.setVisible(false);
        cameraNotChosenPane.setVisible(true);
    }

    /**
     * Saves image to disc and shows it to photo pane.
     *
     * @param image image to show
     */
    public void saveAndShowPhoto(OrdImage image) {
        try {
            image.getDataInFile(IMAGE_CLASSPATH);
            File fileImage = new File(IMAGE_CLASSPATH);
            fileImage.deleteOnExit();

            setPhotoOnPane(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets photo or placeholder to photo pane as background image.
     *
     * @param setPlaceHolder true if set placeholder, false if photo
     */
    public void setPhotoOnPane(boolean setPlaceHolder) {
        String fileName;
        if (setPlaceHolder) {
            fileName = PLACEHOLDER_CLASSPATH;
        } else {
            fileName = IMAGE_CLASSPATH;
        }
        AddItemController.setBackground(fileName, photoPane);
    }

    /**
     * Shows chooser of photo. User can choose photo with .jpg or .png extensions.
     *
     * @return path of chosen photo.
     * @throws FileNotFoundException
     */
    private String choosePhoto() throws FileNotFoundException {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Photos with png or jpg extension", "png", "jpg");
        chooser.setFileFilter(filter);

        chooser.showOpenDialog(null);
        if (chooser.getSelectedFile() == null) {
            throw new FileNotFoundException();
        }
        String name = chooser.getSelectedFile().getName();
        if (!(name.endsWith(".jpg")) && !(name.endsWith(".png"))) {
            throw new FileNotPhotoException();
        }
        return chooser.getSelectedFile().getAbsolutePath();
    }


    /**
     * Shows new frame with image in it as background.
     *
     * @param image class path of image
     * @param title title of new frame
     */
    private void displayImageInNewWindow(String image, String title) {
        String fileName = image;
        InputStream stream = null;
        try {
            stream = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Image similarImage = new Image(stream);
        PhotoAlertBox.displayPhoto(similarImage, title);
    }

    /**
     * Returns id of photo of last selected item if it is camera.
     *
     * @return id
     */
    private int getPhotoIdOfLastSelectedItem() {
        ItemPolygon cameraItem = (ItemPolygon) gardenEditorMainController.canvasPaneController.lastSelectedItem;
        return cameraItem.getItemPolygonReference().getIdPhoto();
    }

    /**
     * Handles users click on load photo button
     *
     * @throws IOException
     * @throws SQLException
     */
    @FXML
    public void loadPhotoButtonClicked() throws IOException, SQLException {
        ItemPolygon cameraItem = (ItemPolygon) gardenEditorMainController.canvasPaneController.lastSelectedItem;
        String fileUrl;
        try {
            fileUrl = choosePhoto();
        } catch (FileNotFoundException e) {
            return;
        } catch (FileNotPhotoException e) {
            AlertBox.display("Not a photo", "This is not supported format of photo.");
            return;
        }

        OrdImage image = photosManipulator.savePhotoToDb(fileUrl, cameraItem);
        saveAndShowPhoto(image);
        setOperationsPaneVisibility(true);
    }

    /**
     * Handles users click on delete photo button
     *
     * @throws SQLException
     */
    @FXML
    public void deletePhotoButtonPressed() throws SQLException {
        ItemPolygon cameraItem = (ItemPolygon) gardenEditorMainController.canvasPaneController.lastSelectedItem;

        Connection connection = Database.getInstance().getConnection();
        connection.setAutoCommit(false);

        photosManipulator.deletePhotoById(connection, cameraItem.getItemPolygonReference().getIdPhoto());

        connection.commit();
        connection.setAutoCommit(true);

        cameraItem.getItemPolygonReference().setIdPhoto(0);
        showCameraChosenPane();
        setPhotoOnPane(true);
        setOperationsPaneVisibility(false);
    }

    /**
     * Shows new frame with image in full size
     */
    @FXML
    public void showInFullSizeButtonPressed() {
        displayImageInNewWindow(IMAGE_CLASSPATH, "Image in full size (click to image to close)");
    }

    /**
     * Handles users click on rotate left photo button
     */
    @FXML
    public void rotateLeftButtonPressed() {
        int photoId = getPhotoIdOfLastSelectedItem();
        photosManipulator.rotatePicture(photoId, -90);
        OrdImage img = photosManipulator.doSelectPhotoByIdQuery(photoId);
        saveAndShowPhoto(img);
    }

    /**
     * Handles users click on rotate right photo button
     */
    @FXML
    public void rotateRightButtonPressed() {
        int photoId = getPhotoIdOfLastSelectedItem();
        photosManipulator.rotatePicture(photoId, 90);
        OrdImage img = photosManipulator.doSelectPhotoByIdQuery(photoId);
        saveAndShowPhoto(img);
    }

    /**
     * Handles users click on turn around photo button
     */
    @FXML
    public void turnAroundButtonPressed() {
        int photoId = getPhotoIdOfLastSelectedItem();
        photosManipulator.rotatePicture(photoId, 180);
        OrdImage img = photosManipulator.doSelectPhotoByIdQuery(photoId);
        saveAndShowPhoto(img);
    }

    /**
     * Shows new frame with similar photo of database to actual photo
     */
    @FXML
    public void showSimilarPhotoButtonPressed() {
        int photoId = getPhotoIdOfLastSelectedItem();
        OrdImage image;
        image = photosManipulator.doSelectSimilarPhoto(photoId);
        if (image == null) {
            return;
        }
        try {
            image.getDataInFile(SIMILAR_IMAGE_CLASSPATH);
            File fileImage = new File(SIMILAR_IMAGE_CLASSPATH);
            fileImage.deleteOnExit();

            displayImageInNewWindow(SIMILAR_IMAGE_CLASSPATH, "Similar photo (click to image to close)");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles users click on mirror photo button
     */
    @FXML
    public void mirrorButtonPressed() {
        int photoId = getPhotoIdOfLastSelectedItem();
        photosManipulator.mirrorPhoto(photoId);
        OrdImage img = photosManipulator.doSelectPhotoByIdQuery(photoId);
        saveAndShowPhoto(img);
    }

}
