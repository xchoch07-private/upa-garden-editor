package cz.vutbr.fit.upa.controller;

import cz.vutbr.fit.upa.AlertBox;
import cz.vutbr.fit.upa.exception.ConnectionException;
import cz.vutbr.fit.upa.exception.SqlStatementNotExecuted;
import cz.vutbr.fit.upa.model.ConnectionData;
import cz.vutbr.fit.upa.Database;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller of ConnectionToDatabase.fxml
 */
public class ConnectionToDatabaseController implements Initializable {

    /**
     * Modal pane from fxml
     */
    @FXML
    private AnchorPane connectionModalPane;

    /**
     * Text field with host from fxml
     */
    @FXML
    private TextField connectionHost;

    /**
     * Text field with port from fxml
     */
    @FXML
    private TextField connectionPort;

    /**
     * Text field with service name from fxml
     */
    @FXML
    private TextField connectionServiceName;

    /**
     * Text field with user name from fxml
     */
    @FXML
    private TextField connectionUser;

    /**
     * Text field with password from fxml
     */
    @FXML
    private PasswordField connectionPassword;

    /**
     * Text with connection failed because of data or user credentials from fxml
     */
    @FXML
    private Text connectionErrText;

    /**
     * Instance of Database
     */
    private Database database;

    /**
     * Data of connection, e.g. username, port, host
     */
    private ConnectionData connectionData;

    /**
     * Logger to log
     */
    private static final Logger log = LoggerFactory.getLogger(ConnectionToDatabaseController.class);

    /**
     * Error message wrong credentials
     */
    private static final String CREDENTIALS_ERR = "Cannot connect with these credentials!";

    /**
     * Error message port is not integer
     */
    private static final String PORT_ERR = "Wrong port format! Port is integer.";

    /**
     * Main controller
     */
    public GardenEditorMainController gardenEditorMainController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        connectionErrText.setVisible(false);
    }

    /**
     * Creates and return instance of connection data and gets texts to it from modal pane
     *
     * @return instance of conn. data
     */
    private ConnectionData createConnectionData() {
        int port;
        try {
            port = Integer.parseInt(connectionPort.getText());
        } catch (NumberFormatException e) {
            log.error("This port is not number.");
            throw new NumberFormatException(e.getMessage());
        }
        return new ConnectionData(connectionHost.getText(), port, connectionServiceName.getText(), connectionUser.getText(), connectionPassword.getText());
    }

    /**
     * Injects main controller
     *
     * @param mainController main controller
     */
    public void setMainController(GardenEditorMainController mainController) {
        this.gardenEditorMainController = mainController;
    }

    /**
     * Handles users mouse click on button close modal pane
     */
    @FXML
    public void closeModalPane() {
        connectionErrText.setVisible(false);
        connectionModalPane.setVisible(false);
        gardenEditorMainController.borderPane.setDisable(false);
    }

    /**
     * Handles users mouse click on button connect
     */
    @FXML
    public void connectButtonClicked() {
        database = Database.getInstance();

        try {
            connectionData = createConnectionData();
            database.connectToDatabase(connectionData);
        } catch (NumberFormatException e) {
            showErrMsg(PORT_ERR);
            return;
        } catch (ConnectionException e) {
            showErrMsg(CREDENTIALS_ERR);
            return;
        }
        connectionErrText.setVisible(false);

        connectionModalPane.setVisible(false);
        gardenEditorMainController.borderPane.setDisable(false);
        gardenEditorMainController.photosOfCameraPaneController.showCameraNotChosenPane();

        try {
            drawItemsFromDatabase();
        } catch (SqlStatementNotExecuted e) {
            AlertBox.display("Connection successful",
                    "Connecting to server " + connectionData.getHost() + ":" + connectionData.getPort() + " was successful. " +
                            "\nCannot load items from your database because it is empty. Please load sql init script in menu tab Database." +
                            "\nWelcome user " + connectionData.getUsername() + ".");
            return;
        }
        AlertBox.display("Connection successful",
                "Connecting to server " + connectionData.getHost() + ":" + connectionData.getPort() + " was successful. " +
                        "\n\nWelcome user " + connectionData.getUsername() + ".");
        gardenEditorMainController.accordionMenu.setDisable(false);
    }

    /**
     * Handles users keyboard click, enter is same as click to connect, similarily escape is close
     *
     * @param e
     */
    @FXML
    public void buttonPressed(KeyEvent e) {
        if (e.getCode() == KeyCode.ENTER) {
            connectButtonClicked();
        } else if (e.getCode() == KeyCode.ESCAPE) {
            closeModalPane();
        }
    }

    /**
     * Sets label as visible and sets error message to it
     *
     * @param msg message
     */
    private void showErrMsg(String msg) {
        connectionErrText.setVisible(true);
        connectionErrText.setText(msg);
    }

    /**
     * Draws all items from database when connection completed or loaded sql init
     *
     * @throws SqlStatementNotExecuted
     */
    private void drawItemsFromDatabase() throws SqlStatementNotExecuted {
        gardenEditorMainController.canvasPaneController.callLoadItemsFromDb();
        gardenEditorMainController.canvasPaneController.draw();
    }
}
