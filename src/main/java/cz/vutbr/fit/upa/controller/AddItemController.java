package cz.vutbr.fit.upa.controller;

import cz.vutbr.fit.upa.model.DatabaseItem;
import cz.vutbr.fit.upa.model.ItemPath;
import cz.vutbr.fit.upa.model.ItemPolygon;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.input.InputEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import oracle.spatial.geometry.JGeometry;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller of AddItemPane.fxml
 */
public class AddItemController implements Initializable {

    /**
     * List for create path from more points
     */
    public List<ItemPath> path;

    /**
     * List for delete all path
     */
    public List<ItemPath> pathsForDelete;

    /**
     * List for save point when objects are creating
     */
    public List<Circle> coordinates;

    /**
     * List for save circles
     */
    public List<Circle> circles;

    /**
     * List for save polygons
     */
    public List<ItemPolygon> polygons;

    /**
     * instance of GardenEditorMainController
     */
    public GardenEditorMainController gardenEditorMainController;

    /**
     * Item path zero
     */
    private DatabaseItem zeroItemPath;

    /**
     * Type of object which is adding
     */
    public String addItemButtonSelect = "";

    /**
     * Variable for drawing new circle
     */
    public Circle newActualCircle = null;

    /**
     * Variable for drawing new polygon
     */
    public Rectangle newActualPolygon = null;

    /**
     * Toggle group for adding different item types
     */
    @FXML
    public ToggleGroup toggleButtons;

    /**
     * Note value in accordion menu
     */
    @FXML
    public TextField note;

    /**
     * Pane for setting clipart images of items as background
     */
    @FXML
    public AnchorPane clipartPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        path = new ArrayList<>();
        coordinates = new ArrayList<>();
        polygons = new ArrayList<>();
        circles = new ArrayList<>();
        pathsForDelete = new ArrayList<>();
        getValueFromSelectedToggleButton();
    }

    /**
     * Gets item type which is adding from button
     */
    public void getValueFromSelectedToggleButton() {
        toggleButtons.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                oldValue.setSelected(true);
                newValue = oldValue;
            }
            if (newValue.getToggleGroup().getSelectedToggle() == null)
                return;

            ToggleButton tb = (ToggleButton) newValue.getToggleGroup().getSelectedToggle();
            if (tb.isSelected()) {
                addItemButtonSelect = tb.getText();
                clearWhenOthrerItemTypeIsAdded(true);
                path.clear();
                coordinates.clear();
                polygons.clear();
                circles.clear();
                showClipart(addItemButtonSelect);
            }
        });
    }

    /**
     * Injects main controller
     *
     * @param mainController main controller
     */
    public void setMainController(GardenEditorMainController mainController) {
        this.gardenEditorMainController = mainController;
    }

    /**
     * Method decides which type of geometry is including item
     *
     * @param event event
     */
    public void addItem(InputEvent event) {
        switch (addItemButtonSelect) {
            case "Path":
            case "Main path":
                addLine(event);
                break;
            case "Camera":
                addPolygonFix(event);
                break;
            case "Green house":
            case "Building":
            case "Flower bed":
                addPolygon(event);
                break;
            case "Tree":
            case "Bush":
                addPointItem(event);
                break;
            case "Irrigation system":
                addCircle(event, true);
                break;
            case "Lake":
                addCircle(event, false);
                break;
        }
    }

    /**
     * This method is for add line. If no point has been created, method create first point of mouse coordinate
     * If first point was created, method create next point by mouse coordinate and create line between two points.
     *
     * @param event event
     */
    public void addLine(InputEvent event) {
        if (event.getEventType() == MouseEvent.MOUSE_CLICKED) {
            if (newActualPolygon != null) {
                gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(newActualPolygon);
                newActualPolygon = null;
            }
            if (newActualCircle != null) {
                gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(newActualCircle);
                newActualCircle = null;
            }
            MouseEvent mouseEvent = (MouseEvent) event;

            if (mouseEvent.getButton() == MouseButton.PRIMARY) {
                addPoint(mouseEvent);
                PathElement newElement;
                if (path.isEmpty()) {
                    path.add(new ItemPath(zeroItemPath, 0));
                    ItemPath lastPath = path.get(path.size() - 1);
                    newElement = new MoveTo(mouseEvent.getX(), mouseEvent.getY());
                    lastPath.getElements().add(newElement);
                } else {
                    ItemPath lastPath = path.get(path.size() - 1);
                    newElement = new LineTo(mouseEvent.getX(), mouseEvent.getY());
                    lastPath.getElements().add(newElement);
                }
                pathsForDelete.add(path.get(path.size() - 1));
                gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(path.get(path.size() - 1));
                gardenEditorMainController.canvasPaneController.canvas.getChildren().add(path.get(path.size() - 1));
            }
        }
    }

    /**
     * This method is for add item circle to canvas. First mouse click creates two points.
     * While mouse is moving second point is moving by mouse coordinate. Distance between two points is radius of circle.
     *
     * @param isIrrigationSystem - if drawing circle is irrigationSystem this method set low opacity for new circle in canvas.
     */
    public void realTimeDrawCircle(boolean isIrrigationSystem) {
        Point2D start = new Point2D(coordinates.get(0).getCenterX(), coordinates.get(0).getCenterY());
        Point2D end = new Point2D(coordinates.get(1).getCenterX(), coordinates.get(1).getCenterY());
        double radius = start.distance(end);
        double radiusMin = radius;
        double radiusMax = radius;

        if (radius < 7) {
            radius = 7;
        }

        if ((coordinates.get(0).getCenterX() - radius < 0) || (coordinates.get(0).getCenterY() - radius < 0)) {
            double radiusMinX = coordinates.get(0).getCenterX();
            double radiusMinY = coordinates.get(0).getCenterY();
            if (radiusMinX < radiusMinY) {
                radiusMin = radiusMinX;
            } else {
                radiusMin = radiusMinY;
            }
        }

        if ((coordinates.get(0).getCenterX() + radius > 840) || (coordinates.get(0).getCenterY() + radius > 720)) {
            double radiusMaxX = 840 - coordinates.get(0).getCenterX();
            double radiusMaxY = 720 - coordinates.get(0).getCenterY();

            if (radiusMaxX > radiusMaxY) {
                radiusMax = radiusMaxY;
            } else {
                radiusMax = radiusMaxX;
            }
        }

        if ((coordinates.get(0).getCenterX() - radius < 0) || (coordinates.get(0).getCenterY() - radius < 0) || (coordinates.get(0).getCenterX() + radius > 840) || (coordinates.get(0).getCenterY() + radius > 720)) {
            if (radiusMax > radiusMin) {
                radius = radiusMin;
            } else {
                radius = radiusMax;
            }
        }

        newActualCircle = new Circle(coordinates.get(0).getCenterX(), coordinates.get(0).getCenterY(), radius);
        if (isIrrigationSystem) {
            newActualCircle.setOpacity(0.3);
        }

        newActualCircle.setFill(Color.web("#ed4b00"));
        gardenEditorMainController.canvasPaneController.canvas.getChildren().add(newActualCircle);
    }


    /**
     * This method is for drawing polygon into canvas. First mouse click create two points from mouse coordinate.
     * While mouse is moving second point is moving. Other points are calculated from it.
     */
    public void realTimeDrawPolygon() {
        double x = 0;
        double y = 0;
        double point0X = coordinates.get(0).getCenterX();
        double point0Y = coordinates.get(0).getCenterY();
        double point1X = coordinates.get(1).getCenterX();
        double point1Y = coordinates.get(1).getCenterY();

        if (point0X < 0) {
            point0X = 0;
        }

        if (point0Y < 0) {
            point0Y = 0;
        }

        if (point1X < 0) {
            point1X = 0;
        }

        if (point1Y < 0) {
            point1Y = 0;
        }

        double width = Math.abs(point1X - point0X);
        double height = Math.abs(point1Y - point0Y);

        if (coordinates.get(0).getCenterX() < coordinates.get(1).getCenterX()) {
            x = coordinates.get(0).getCenterX();
        } else {
            x = coordinates.get(1).getCenterX();
        }

        if (coordinates.get(0).getCenterY() < coordinates.get(1).getCenterY()) {
            y = coordinates.get(0).getCenterY();
        } else {
            y = coordinates.get(1).getCenterY();
        }

        //control border of canvas
        if (x < 0) {
            x = 0;
        }

        if (y < 0) {
            y = 0;
        }

        if ((x + width) > 840) {
            width = 840 - x;
        }

        if ((y + height) > 720) {
            height = 720 - y;
        }

        newActualPolygon = new Rectangle(x, y, width, height);
        newActualPolygon.setFill(Color.web("#ed4b00"));
        gardenEditorMainController.canvasPaneController.canvas.getChildren().add(newActualPolygon);

    }

    /**
     * If the second item starts to draw and one has already been drawn before, it has not been saved.
     * So the first item is deleted so that the current one can be rendered.
     *
     * @param isCirclesPoints
     */
    public void clearWhenOthrerItemTypeIsAdded(boolean isCirclesPoints) {
        if (newActualPolygon != null) {
            gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(newActualPolygon);
            newActualPolygon = null;
        }
        if (newActualCircle != null) {
            gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(newActualCircle);
            newActualCircle = null;
        }
        if (!(pathsForDelete.isEmpty())) {
            for (ItemPath line : pathsForDelete) {
                gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(line);
            }
            path.clear();
        }
        if (isCirclesPoints) {
            if (!(circles.isEmpty())) {
                for (Circle circle : circles) {
                    gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(circle);
                }
                circles.clear();
            }
        }
    }

    /**
     * Creates fix size item camera
     *
     * @param event event
     */
    public void addPolygonFix(InputEvent event) {
        if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
            clearWhenOthrerItemTypeIsAdded(true);

            if (!coordinates.isEmpty()) {
                gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(coordinates.get(1));
                coordinates.remove(1);
                gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(coordinates.get(0));
                coordinates.remove(0);
            }


            addPoint((MouseEvent) event);
            Circle point = new Circle(((MouseEvent) event).getX() + 20, ((MouseEvent) event).getY() + 20, 2.0f, Color.TRANSPARENT);
            coordinates.add(point);
            gardenEditorMainController.canvasPaneController.canvas.getChildren().add(point);

            realTimeDrawPolygon();
        }
    }

    /**
     * Method adds new polygon item to canvas
     *
     * @param event event
     */
    public void addPolygon(InputEvent event) {
        if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
            clearWhenOthrerItemTypeIsAdded(true);
            addPoint((MouseEvent) event);
            addPoint((MouseEvent) event);
            realTimeDrawPolygon();
        } else if (event.getEventType() == MouseEvent.MOUSE_DRAGGED) {
            gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(newActualPolygon);
            gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(coordinates.get(1));
            coordinates.remove(1);
            addPoint((MouseEvent) event);
            realTimeDrawPolygon();
        } else if (event.getEventType() == MouseEvent.MOUSE_RELEASED) {
            gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(coordinates.get(1));
            gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(coordinates.get(0));
            coordinates.remove(1);
            coordinates.remove(0);
        }
    }

    /**
     * Method adding new circle item to canvas
     *
     * @param event              mouse event
     * @param isIrrigationSystem different type of circle for irrigation system
     */
    public void addCircle(InputEvent event, boolean isIrrigationSystem) {
        if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
            clearWhenOthrerItemTypeIsAdded(true);
            addPoint((MouseEvent) event);
            addPoint((MouseEvent) event);
            realTimeDrawCircle(isIrrigationSystem);
        } else if (event.getEventType() == MouseEvent.MOUSE_DRAGGED) {
            gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(newActualCircle);
            gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(coordinates.get(1));
            coordinates.remove(1);
            addPoint((MouseEvent) event);
            realTimeDrawCircle(isIrrigationSystem);
        } else if (event.getEventType() == MouseEvent.MOUSE_RELEASED) {
            gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(coordinates.get(1));
            gardenEditorMainController.canvasPaneController.canvas.getChildren().remove(coordinates.get(0));
            coordinates.remove(1);
            coordinates.remove(0);
        }
    }

    /**
     * Method adding new point to canvas
     *
     * @param event event
     */
    public void addPointItem(InputEvent event) {
        if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
            addPoint((MouseEvent) event);
            clearWhenOthrerItemTypeIsAdded(false);

            if (!((coordinates.get(0).getCenterX() - 7 < 0) || (coordinates.get(0).getCenterY() - 7 < 0) || (coordinates.get(0).getCenterX() + 7 > 840) || (coordinates.get(0).getCenterY() + 7 > 720))) {
                circles.add(new Circle(coordinates.get(0).getCenterX(), coordinates.get(0).getCenterY(), 7));
                circles.get(circles.size() - 1).setFill(Color.web("#ed4b00"));
                gardenEditorMainController.canvasPaneController.canvas.getChildren().add(circles.get(circles.size() - 1));
            }
            coordinates.remove(0);
        }
    }

    /**
     * Method saves drew item to database
     */
    @FXML
    public void saveItemToDatabase() {
        DatabaseItem newItemToDatabase = null;
        int i = 1;
        int lastId = gardenEditorMainController.itemDatabaseManipulator.getLastId();
        List<Double> points = new ArrayList<>();
        List<Integer> sdoElemInfo = new ArrayList<>();
        JGeometry geometry = null;
        int gTypeObjectType = 0;

        if (!path.isEmpty()) {
            for (ItemPath newItemPath : path) {
                if (newItemPath.getElements().size() > 1) {
                    sdoElemInfo.add(i);
                    sdoElemInfo.add(2);
                    sdoElemInfo.add(1);

                    for (PathElement element : newItemPath.getElements()) {
                        if (element instanceof MoveTo) {
                            points.add(((MoveTo) element).getX());
                            points.add(((MoveTo) element).getY());
                        } else if (element instanceof LineTo) {
                            points.add(((LineTo) element).getX());
                            points.add(((LineTo) element).getY());
                        }
                        i++;
                    }
                }
            }
            gTypeObjectType = JGeometry.GTYPE_CURVE;
        }

        if (newActualCircle != null) {
            //generate points for circles
            gTypeObjectType = JGeometry.GTYPE_POLYGON;

            sdoElemInfo.add(1);
            sdoElemInfo.add(1003);
            sdoElemInfo.add(4);

            int radius = (int) newActualCircle.getRadius();

            points.add(newActualCircle.getCenterX());
            points.add(newActualCircle.getCenterY() - radius);

            points.add(newActualCircle.getCenterX());
            points.add(newActualCircle.getCenterY() + radius);

            points.add(newActualCircle.getCenterX() + radius);
            points.add(newActualCircle.getCenterY());
        }

        if (newActualPolygon != null) {
            gTypeObjectType = JGeometry.GTYPE_POLYGON;

            sdoElemInfo.add(1);
            sdoElemInfo.add(1003);
            sdoElemInfo.add(1);

            points.add(newActualPolygon.getX());
            points.add(newActualPolygon.getY());

            points.add(newActualPolygon.getX() + newActualPolygon.getWidth());
            points.add(newActualPolygon.getY());

            points.add(newActualPolygon.getX() + newActualPolygon.getWidth());
            points.add(newActualPolygon.getY() + newActualPolygon.getHeight());

            points.add(newActualPolygon.getX());
            points.add(newActualPolygon.getY() + newActualPolygon.getHeight());

            points.add(newActualPolygon.getX());
            points.add(newActualPolygon.getY());
        }

        if (!(circles.isEmpty())) {
            if (circles.size() == 1) {
                //point
                gTypeObjectType = JGeometry.GTYPE_POINT;
                Circle point = circles.get(0);
                points.add(point.getCenterX());
                points.add(point.getCenterY());
            } else {
                //multipoint
                gTypeObjectType = JGeometry.GTYPE_MULTIPOINT;
                int positionOfEachPoint = 1;

                for (Circle c : circles) {
                    sdoElemInfo.add(positionOfEachPoint);
                    sdoElemInfo.add(1);
                    sdoElemInfo.add(1);

                    points.add(c.getCenterX());
                    points.add(c.getCenterY());

                    positionOfEachPoint += 2;
                }
            }
        }

        if (gTypeObjectType == JGeometry.GTYPE_POINT) {
            geometry = new JGeometry(points.get(0), points.get(1), 0);
        } else if (!(points.isEmpty() || sdoElemInfo.isEmpty())) {
            int[] valuesOfSdoElemInfo = sdoElemInfo.stream().mapToInt(d -> d).toArray();
            double[] valuesOfPoints = points.stream().mapToDouble(d -> d).toArray();
            geometry = new JGeometry(gTypeObjectType, 0, valuesOfSdoElemInfo, valuesOfPoints);
        }

        int id = lastId + 1;
        newItemToDatabase = new DatabaseItem(id, addItemButtonSelect + id, note.getText(), geometry, addItemButtonSelect, 0);

        gardenEditorMainController.itemDatabaseManipulator.saveItemToDatabaseSQL((DatabaseItem) newItemToDatabase);
        gardenEditorMainController.canvasPaneController.callLoadItemsFromDb();
        gardenEditorMainController.canvasPaneController.draw();
        path.clear();
        circles.clear();
        note.clear();
    }

    /**
     * Method adds auxiliary point for drawing items
     *
     * @param event
     */
    public void addPoint(MouseEvent event) {
        Circle point = new Circle(event.getX(), event.getY(), 2.0f, Color.TRANSPARENT);
        coordinates.add(point);
        gardenEditorMainController.canvasPaneController.canvas.getChildren().add(point);
    }

    /**
     * Gets correct clipart image of chosen item and sets it as background of clipart-pane
     *
     * @param toggleText text of toggle button - item name
     */
    private void showClipart(String toggleText) {
        String fileName = "./src/main/resources/images/";
        String name = toggleText.replaceFirst(" ", "_");
        fileName += name;
        fileName += ".png";

        setBackground(fileName, clipartPane);
    }

    /**
     * Sets image with url of first parameter as background of pane of second parameter
     *
     * @param fileName    url of image
     * @param clipartPane clipart pane to set background
     */
    static void setBackground(String fileName, AnchorPane clipartPane) {
        InputStream stream = null;
        try {
            stream = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Image image = new Image(stream);
        BackgroundSize backgroundSize = new BackgroundSize(image.getWidth(), image.getHeight(), false, false, true, false);
        BackgroundImage background = new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                backgroundSize);
        clipartPane.setBackground(new Background(background));
    }
}
