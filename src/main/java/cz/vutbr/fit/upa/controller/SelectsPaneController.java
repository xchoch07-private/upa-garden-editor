package cz.vutbr.fit.upa.controller;

import cz.vutbr.fit.upa.AlertBox;
import cz.vutbr.fit.upa.model.DatabaseItem;
import cz.vutbr.fit.upa.model.ItemCircle;
import cz.vutbr.fit.upa.model.ItemPath;
import cz.vutbr.fit.upa.model.ItemPolygon;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.shape.Shape;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller of SelectPane.fxml
 */
public class SelectsPaneController implements Initializable {

    /**
     * Label of district value to show in canvas
     */
    @FXML
    public Label districtValue;

    /**
     * Label of content value to show in canvas
     */
    @FXML
    public Label contentValue;

    /**
     * Input field of distance value
     */
    @FXML
    public TextField distanceValue;

    /**
     * Toggle button for choosing second item in canvas
     */
    @FXML
    public ToggleButton secondItemButton;

    /**
     * Label of distance two items to show in canvas
     */
    @FXML
    public Label distanceOfTwoItemsValue;

    /**
     * Input field of count of nearest items
     */
    @FXML
    public TextField neighborsValue;

    /**
     * Main Controller
     */
    public GardenEditorMainController gardenEditorMainController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Injects main controller
     *
     * @param mainController main controller
     */
    public void setMainController(GardenEditorMainController mainController) {
        this.gardenEditorMainController = mainController;
    }

    /**
     * Auxiliary method for content and district
     *
     * @param selectedItem actual selected item in canvas
     */
    public void loadGeometryOfItem(Shape selectedItem) {
        loadContentOfItem(selectedItem);
        loadDistrictOfItem(selectedItem);
    }

    /**
     * This method loads district value of selected item and writes it to Label
     *
     * @param item selected item
     */
    public void loadDistrictOfItem(Shape item) {
        if (item instanceof ItemPolygon) {
            ItemPolygon polygon = (ItemPolygon) item;
            if (!polygon.getItemPolygonReference().getItemType().equals("Camera")) {
                districtValue.setText(gardenEditorMainController.itemDatabaseManipulator.getDistrictOfItemSQL(polygon.getItemIdFromDatabase()) + " m");
            } else {
                districtValue.setText("");
            }
        }

        if (item instanceof ItemCircle) {
            ItemCircle circle = (ItemCircle) item;
            districtValue.setText(gardenEditorMainController.itemDatabaseManipulator.getDistrictOfItemSQL(circle.getItemIdFromDatabase()) + " m");
        }

        if (item instanceof ItemPath) {
            ItemPath path = (ItemPath) item;
            districtValue.setText(gardenEditorMainController.itemDatabaseManipulator.getDistrictOfItemSQL(path.getItemIdFromDatabase()) + " m");
        }
    }

    /**
     * This method loads content value of selected item and writes ti to Label
     *
     * @param item selected item
     */
    public void loadContentOfItem(Shape item) {
        if (item instanceof ItemPolygon) {
            ItemPolygon polygon = (ItemPolygon) item;
            if (!polygon.getItemPolygonReference().getItemType().equals("Camera")) {
                contentValue.setText(gardenEditorMainController.itemDatabaseManipulator.getAreaOfItemSQL(polygon.getItemIdFromDatabase()) + " \u33A1");
            } else {
                contentValue.setText("");
            }
        }

        if (item instanceof ItemCircle) {
            ItemCircle circle = (ItemCircle) item;
            contentValue.setText(gardenEditorMainController.itemDatabaseManipulator.getAreaOfItemSQL(circle.getItemIdFromDatabase()) + " \u33A1");
        }

        if (item instanceof ItemPath) {
            ItemPath path = (ItemPath) item;
            contentValue.setText(gardenEditorMainController.itemDatabaseManipulator.getAreaOfItemSQL(path.getItemIdFromDatabase()) + " \u33A1");
        }
    }

    /**
     * This method shows all items in canvas from selected item, that are distant from specified value
     */
    @FXML
    public void getDistanceFromObject() {
        List<DatabaseItem> itemList = new ArrayList<>();
        int distance;

        if (gardenEditorMainController.canvasPaneController.lastSelectedItem == null) {
            AlertBox.display("Not chosen item", "Please, choose some item.");
            return;
        }

        try {
            distance = Integer.parseInt(distanceValue.getText());
        } catch (NumberFormatException e) {
            AlertBox.display("Incorrect value", "You can write only number.");
            distanceValue.setText("");
            return;
        }

        if (distance < 0) {
            AlertBox.display("Incorrect value", "Please, choose positive number.");
            return;
        }

        if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPolygon) {
            ItemPolygon polygon = (ItemPolygon) gardenEditorMainController.canvasPaneController.lastSelectedItem;
            itemList = gardenEditorMainController.itemDatabaseManipulator.getDistanceFromObjectSQL(polygon.getItemPolygonReference().geometry, distance);
        }

        if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemCircle) {
            ItemCircle circle = (ItemCircle) gardenEditorMainController.canvasPaneController.lastSelectedItem;
            itemList = gardenEditorMainController.itemDatabaseManipulator.getDistanceFromObjectSQL(circle.getItemCircleReference().geometry, distance);
        }

        if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPath) {
            ItemPath path = (ItemPath) gardenEditorMainController.canvasPaneController.lastSelectedItem;
            itemList = gardenEditorMainController.itemDatabaseManipulator.getDistanceFromObjectSQL(path.getItemPathReference().geometry, distance);
        }

        gardenEditorMainController.canvasPaneController.itemList = itemList;
        gardenEditorMainController.canvasPaneController.clearMap();
        gardenEditorMainController.canvasPaneController.draw();
    }

    /**
     * This method allows to select second item for instance in canvas
     */
    @FXML
    public void secondItemButtonChange() {
        if (gardenEditorMainController.canvasPaneController.lastSelectItemForDistance != null) {
            gardenEditorMainController.canvasPaneController.setOriginalColor(gardenEditorMainController.canvasPaneController.lastSelectItemForDistance);
        }
    }

    /**
     * This method shows distance value of two selected items in Label
     *
     * @param lastSelectedItem          first selected item for distance
     * @param lastSelectItemForDistance second selected item for distance
     */
    public void getDistanceOfTwoItems(Shape lastSelectedItem, Shape lastSelectItemForDistance) {
        int idOfLastSelectedItem = getIdOfItem(lastSelectedItem);
        int idOfLastSelectItemForDistance = getIdOfItem(lastSelectItemForDistance);

        distanceOfTwoItemsValue.setText(gardenEditorMainController.itemDatabaseManipulator.getDistanceOfTwoItemsSQL(idOfLastSelectedItem, idOfLastSelectItemForDistance) + " m");
    }

    /**
     * Auxiliary method for getting id of some item
     *
     * @param lastSelectItemForDistance selected item
     * @return id of selected item
     */
    private int getIdOfItem(Shape lastSelectItemForDistance) {
        int id = 0;
        if (lastSelectItemForDistance instanceof ItemPolygon) {
            ItemPolygon itemPolygon = (ItemPolygon) lastSelectItemForDistance;
            id = itemPolygon.getItemPolygonReference().id;
        }

        if (lastSelectItemForDistance instanceof ItemCircle) {
            ItemCircle itemCircle = (ItemCircle) lastSelectItemForDistance;
            id = itemCircle.getItemCircleReference().id;
        }

        if (lastSelectItemForDistance instanceof ItemPath) {
            ItemPath itemPath = (ItemPath) lastSelectItemForDistance;
            id = itemPath.getItemPathReference().id;
        }
        return id;
    }

    /**
     * This method show all items in canvas, that are covered by Irrigation system
     */
    @FXML
    public void getAllItemsCoveredByIrrigationSystem() {

        if (gardenEditorMainController.canvasPaneController.lastSelectedItem == null) {
            AlertBox.display("Not chosen Irrigation system", "Please, choose some Irrigation system.");
            return;
        }

        if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPolygon || gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPath) {
            AlertBox.display("Not chosen Irrigation system", "Please, choose some Irrigation system.");
            return;
        }

        if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemCircle) {
            String itemType = ((ItemCircle) gardenEditorMainController.canvasPaneController.lastSelectedItem).getItemCircleReference().getItemType();
            int id = ((ItemCircle) gardenEditorMainController.canvasPaneController.lastSelectedItem).getItemCircleReference().id;
            if (itemType.equals("Irrigation system")) {
                List<DatabaseItem> itemList;
                itemList = gardenEditorMainController.itemDatabaseManipulator.getAllItemsCoveredByIrrigationSystemSQL(id);
                gardenEditorMainController.canvasPaneController.itemList = itemList;
                gardenEditorMainController.canvasPaneController.clearMap();
                gardenEditorMainController.canvasPaneController.draw();
            } else {
                AlertBox.display("Irrigation system", "Please, choose Irrigation system");
            }
        }
    }

    /**
     * This method shows n nearest items in canvas from selected item
     * Value of n is from TextField
     */
    @FXML
    public void getNNearestNeighbors() {
        List<DatabaseItem> itemList = new ArrayList<>();
        int count;

        if (gardenEditorMainController.canvasPaneController.lastSelectedItem == null) {
            AlertBox.display("Not chosen item", "Please, choose some item.");
            return;
        }

        try {
            count = Integer.parseInt(neighborsValue.getText()) + 1;
        } catch (NumberFormatException e) {
            AlertBox.display("Incorrect value", "You can write only number.");
            neighborsValue.setText("");
            return;
        }

        if (count < 0) {
            AlertBox.display("Incorrect value", "Please, choose positive number.");
            return;
        }

        if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPolygon) {
            ItemPolygon polygon = (ItemPolygon) gardenEditorMainController.canvasPaneController.lastSelectedItem;
            itemList = gardenEditorMainController.itemDatabaseManipulator.getNNearestNeighborsSQL(polygon.getItemPolygonReference().id, String.valueOf(count));
        }

        if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemCircle) {
            ItemCircle circle = (ItemCircle) gardenEditorMainController.canvasPaneController.lastSelectedItem;
            itemList = gardenEditorMainController.itemDatabaseManipulator.getNNearestNeighborsSQL(circle.getItemCircleReference().id, String.valueOf(count));
        }

        if (gardenEditorMainController.canvasPaneController.lastSelectedItem instanceof ItemPath) {
            ItemPath path = (ItemPath) gardenEditorMainController.canvasPaneController.lastSelectedItem;
            itemList = gardenEditorMainController.itemDatabaseManipulator.getNNearestNeighborsSQL(path.getItemPathReference().id, String.valueOf(count));
        }

        gardenEditorMainController.canvasPaneController.itemList = itemList;
        gardenEditorMainController.canvasPaneController.clearMap();
        gardenEditorMainController.canvasPaneController.draw();
    }

    /**
     * This method resets values in Select pane to default
     */
    @FXML
    public void resetSelectPane() {
        districtValue.setText("");
        contentValue.setText("");
        distanceValue.clear();
        neighborsValue.clear();
        secondItemButton.setSelected(false);
        distanceOfTwoItemsValue.setText("");
        gardenEditorMainController.canvasPaneController.clearMap();
        gardenEditorMainController.canvasPaneController.callLoadItemsFromDb();
        gardenEditorMainController.canvasPaneController.draw();
        gardenEditorMainController.canvasPaneController.lastSelectedItem = null;
    }
}
