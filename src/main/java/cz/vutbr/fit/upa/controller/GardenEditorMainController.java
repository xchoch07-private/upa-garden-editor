package cz.vutbr.fit.upa.controller;

import cz.vutbr.fit.upa.AlertBox;
import cz.vutbr.fit.upa.Database;
import cz.vutbr.fit.upa.GardenEditor;
import cz.vutbr.fit.upa.exception.FileNotSqlException;
import cz.vutbr.fit.upa.model.ItemConvertAndProperties;
import cz.vutbr.fit.upa.model.ItemDatabaseManipulator;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TitledPane;
import javafx.scene.input.InputEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller of GardenEditorMain.fxml
 */
public class GardenEditorMainController implements Initializable {

    /**
     * Main border pane of Garden editor
     */
    @FXML
    public BorderPane borderPane;

    /**
     * Add item Titled pane in accordion menu
     */
    @FXML
    public TitledPane addItemTitledPane;

    /**
     * Modify item Titled pane in accordion menu
     */
    @FXML
    public TitledPane modifyItemTitledPane;

    /**
     * Modal pane for connect to database
     */
    @FXML
    private AnchorPane modalPane;

    /**
     * Main accordion menu
     */
    @FXML
    public Accordion accordionMenu;

    /**
     * Value of TitledPanes in accordion menu
     */
    public String selectedAccordionMenu = "";

    /**
     * Logger for logging
     */
    private static final Logger log = LoggerFactory.getLogger(ConnectionToDatabaseController.class);

    /**
     * Instance of {@link Database}
     */
    private Database database;

    /**
     * Instance of {@link CanvasController}
     */
    public CanvasController canvasPaneController;

    /**
     * Instance of {@link AddItemController}
     */
    public AddItemController addItemPaneController;

    /**
     * Instance of {@link ModifyItemController}
     */
    public ModifyItemController modifyItemPaneController;

    /**
     * Instance of {@link ConnectionToDatabaseController}
     */
    public ConnectionToDatabaseController modalPaneController;

    /**
     * Instance of {@link SelectsPaneController}
     */
    public SelectsPaneController selectsPaneController;

    /**
     * Instance of {@link PhotosOfCameraController}
     */
    public PhotosOfCameraController photosOfCameraPaneController;

    /**
     * Instance of {@link ItemDatabaseManipulator}
     */
    public ItemDatabaseManipulator itemDatabaseManipulator = new ItemDatabaseManipulator();

    /**
     * Instance of {@link ItemConvertAndProperties}
     */
    public ItemConvertAndProperties itemConvertAndProperties = new ItemConvertAndProperties();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //inject controllers, set this as main controller to them
        this.itemDatabaseManipulator.setMainController(this);
        this.itemConvertAndProperties.setMainController(this);
        this.canvasPaneController.setMainController(this);
        this.addItemPaneController.setMainController(this);
        this.modifyItemPaneController.setMainController(this);
        this.modalPaneController.setMainController(this);
        this.selectsPaneController.setMainController(this);
        this.photosOfCameraPaneController.setMainController(this);

        accordionMenu.expandedPaneProperty().addListener((ov, t, expandedPane) -> {
            if (expandedPane != null)
                switch (expandedPane.getText()) {
                    case "Photo":
                        selectedAccordionMenu = "Photo";
                        break;
                    case "Add Item":
                        selectedAccordionMenu = "Add Item";
                        break;
                    case "Modify Item":
                        selectedAccordionMenu = "Modify Item";
                        break;
                    case "Selects":
                        selectedAccordionMenu = "Selects";
                        break;
                    default:
                        selectedAccordionMenu = "";
                        break;
                }
            if (Database.getInstance().getConnection() != null) {
                canvasPaneController.clearMap();
                canvasPaneController.callLoadItemsFromDb();
                canvasPaneController.draw();
            }
            canvasPaneController.lastSelectedItem = null;
            photosOfCameraPaneController.showCameraNotChosenPane();

            canvasPaneController.lastSelectItemForDistance = null;
            selectsPaneController.districtValue.setText("");
            selectsPaneController.contentValue.setText("");

        });

        accordionMenu.setDisable(true);
    }

    /**
     * Shows help from menu item Help
     */
    @FXML
    public void showHelp() {
        Stage window = new Stage();
        StackPane root = new StackPane();
        window.setResizable(false);
        root.setId("Help");

        Scene menuScene = new Scene(root, 1100, 650);
        menuScene.getStylesheets().add("fxml/style.css");
        window.setScene(menuScene);
        window.show();
    }

    /**
     * Shows about us from menu item About
     */
    @FXML
    public void showAbout() {
        AlertBox.display("About us", "Authors:\n" + "Ondřej Novák - xnovak2b\n" + "Lukáš Ondrák - xondra49\n" + "Tomáš Chocholatý - xchoch07");
    }

    /**
     * Closes program from menu item Close
     */
    @FXML
    public void closeProgram() {
        File fileImage = new File(GardenEditor.IMAGE_CLASSPATH);
        fileImage.delete();
        Platform.exit();
        System.exit(0);
    }

    /**
     * Open connection pane from menu item Connect
     */
    @FXML
    public void openConnectionPane() {
        borderPane.setDisable(true);
        modalPane.setDisable(false);
        modalPane.setVisible(true);
    }

    /**
     * Create new garden (meaning: delete all data from database) from menu item New garden
     */
    @FXML
    public void getNewGarden() {
        database = Database.getInstance();
        if (database.getConnection() == null) {
            log.error("No database connected!");
            AlertBox.display("No database connected", "New garden cannot be created.\nYou are not connected to database.");
            return;
        }

        Alert alert = new Alert(Alert.AlertType.NONE, "Are you sure?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("New garden");
        alert.showAndWait();
        if (alert.getResult() == ButtonType.YES) {
            itemDatabaseManipulator.deleteAllFromTableCanvasItemsSQL();
            itemDatabaseManipulator.deleteAllFromTableCameraPhotosSQL();
            photosOfCameraPaneController.showCameraNotChosenPane();
            canvasPaneController.clearMap();
        }
    }

    /**
     * Decides which TitledPane is active and calls relevant method
     *
     * @param event event
     */
    public void selectCanvasHandlerType(InputEvent event) {
        switch (this.selectedAccordionMenu) {
            case "":
                break;
            case "Photo":
                break;
            case "Add Item":
                addItemPaneController.addItem(event);
                break;
            case "Modify Item":
                modifyItemPaneController.modifyItem(event);
                break;
            case "Selects":
                break;
        }
    }

    /**
     * Loads SQL file from pc
     *
     * @return path of SQL file
     * @throws FileNotFoundException file not exists
     */
    protected String getSQLFile() throws FileNotFoundException {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "SQL file", "sql");
        chooser.setFileFilter(filter);

        chooser.showOpenDialog(null);
        if (chooser.getSelectedFile() == null) {
            throw new FileNotFoundException();
        }
        log.info("Opening file: " + chooser.getSelectedFile().getName());

        if (!chooser.getSelectedFile().getName().endsWith(".sql")) {
            log.error("Chosen file is not sql.");
            throw new FileNotSqlException();
        }
        return chooser.getSelectedFile().getAbsolutePath();
    }

    /**
     * Parses loaded SQL file
     *
     * @param file chosen SQL file
     * @return SQL file as List of Strings
     * @throws IOException file not found or is empty
     */
    protected List<String> parseSQLFile(String file) throws IOException {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
        StringBuilder exec = new StringBuilder();
        String line = "";
        while ((line = reader.readLine()) != null) {
            exec.append(line);
        }
        return Arrays.asList(exec.toString().split(";"));
    }

    /**
     * Loads init script to database
     *
     * @param event event
     * @throws IOException file may not exists
     */
    @FXML
    public void loadSqlInit(ActionEvent event) throws IOException {
        database = Database.getInstance();
        if (database.getConnection() == null) {
            log.error("No database connected!");
            AlertBox.display("No database connected", "Sql script cannot be loaded.\nYou are not connected to database.");
            return;
        }

        String filePath;
        try {
            filePath = getSQLFile();
        } catch (FileNotSqlException e) {
            AlertBox.display("Wrong file", "File is not sql file. \nChoose correct file with .sql extension, please.");
            return;
        } catch (FileNotFoundException e) {
            return;
        }
        List<String> sqlFileToSend = parseSQLFile(filePath);
        database.sendSqlToDatabase(sqlFileToSend);

        log.info("Sql script from file " + filePath + " was loaded to database.");
        AlertBox.display("Sql script successfully loaded", "Sql script from file " + filePath + " was loaded to database.");

        this.canvasPaneController.callLoadItemsFromDb();
        this.canvasPaneController.draw();

        photosOfCameraPaneController.showCameraNotChosenPane();
    }

    /**
     * Handles user event - mouse click on on add item titled-pane
     *
     * @param mouseEvent event caused by mouse click
     */
    @FXML
    public void addItemTitledPaneClicked(MouseEvent mouseEvent) {
        if (!addItemTitledPane.isExpanded() && addItemPaneController.toggleButtons.getSelectedToggle() != null) {
            addItemPaneController.toggleButtons.getSelectedToggle().setSelected(false);
            this.selectedAccordionMenu = "";
        }
    }

    /**
     * Handles user event - mouse click on on modify item titled-pane
     *
     * @param mouseEvent event caused by mouse click
     */
    @FXML
    public void modifyItemTitledPaneClicked(MouseEvent mouseEvent) {
        if (!modifyItemTitledPane.isExpanded() && modifyItemPaneController.toggleButtonsModify.getSelectedToggle() != null) {
            modifyItemPaneController.toggleButtonsModify.getSelectedToggle().setSelected(false);
            this.selectedAccordionMenu = "";
        }
    }
}
