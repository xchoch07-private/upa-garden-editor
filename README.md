# UPA Garden Editor

Aplikace slouží jako grafický editor pro návrh zahrad. Na území zahrady je možné vkládat různé objekty, které mají
podporu následné editace. Objekty je možné po mapě libovolně přemisťovat, navíc lze měnít jejich velikost či jimi rotovat.
Nad tímto návrhem lze pak provádět různé operace, jako je například kontrola, jaké území pokrývají zavlažovače. Dalšími
užitečnými operacemi jsou různé geometrické informace od obvodu či obsahu až po výpis vzdáleností.

Pro potřeby kontroly pozemku se v aplikaci nachází objekt kamera. Kameře je možné přiřazovat obrázek a tím si určit
pohled zahrady, který má kamera pokrývat. S přiřazenými obrázky lze manipulovat pomocí operací Rotate, Mirror či Turn around.
Je zde i implemtováno vyhledávání obrázku na základě podobnosti. 

Součástí tohoto projektu je i inicializační [SQL skript](initScript.sql), který slouží pro vytvoření a naplnění databáze
daty. Dále je součástí [obrázek](UPA_ER_diagram.png) ukazující schéma databáze.

## Stažení projektu

Projekt je k nalezení na stránkách [GitLabu](https://gitlab.com/xchoch07-private/upa-garden-editor), kde stačí projekt
stáhnout pomocí **Download** tlačítka.

Nebo za pomocí příkazu níže v bash konzoli.
```
$ git clone https://gitlab.com/xchoch07-private/upa-garden-editor.git
```

## Sestavení projektu
### IDE postup
Před samotným importem projektu do IDE nejprve navštivte soubor [settings.xml](settings.xml). Je v něm krátký popis, co
se s ním musí udělat, aby projekt fungoval správně. 

Poté stačí projekt importovat do IDE jako Maven project, tedy importovat soubor [pom.xml](pom.xml). Po importu se
automaticky začnou stahovat všechny potřebné knihovny. Projekt běží na **Java 1.8**, je tedy nutné ji v IDE nastavit. 
Dále je nutné nastavit **Project language level** na hodnotu `7 - Diamonds, ARM, multi-catch etc.`, který se nachází v 
**Project structure**.

Nakonec stačí celý projekt spustit pomocí souboru [GardenEditor.java](src/main/java/cz/vutbr/fit/upa/GardenEditor.java).

### Maven

Ke spuštění aplikace je potřeba nástroj [Maven](https://maven.apache.org/). Rovněž je zde nutné správně nakonfigurovat
soubor [settings.xml](settings.xml), aby se správně stáhly všechny potřebné knihvony.

Pro kompilaci a následné spuštění aplikace je třeba zadat do Command line následující sekvenci přákazů.
```
$ mvn install
$ mvn exec:java -Dexec.mainClass="cz.vutbr.fit.upa.GardenEditor"
```

#### Autoři:
* Ondřej Novák - xnovak2b
* Lukáš Ondrák - xondra49
* Tomáš Chocholatý - xchoch07
