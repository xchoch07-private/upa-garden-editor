ALTER SESSION SET NLS_DATE_FORMAT='DD-MM-YYYY';

DROP TABLE canvas_items CASCADE CONSTRAINT;
DROP TABLE camera_photos CASCADE CONSTRAINT;

CREATE TABLE camera_photos (
    id_photo NUMBER NOT NULL,
    photo ORDSYS.ORDImage,
    photo_ac ORDSYS.SI_AverageColor,
    photo_pc ORDSYS.SI_PositionalColor,                                    
    photo_si ORDSYS.SI_StillImage,
    photo_ch ORDSYS.SI_ColorHistogram,
    photo_tx ORDSYS.SI_Texture
);

ALTER TABLE camera_photos ADD CONSTRAINT PK_camera_photos PRIMARY KEY (id_photo);

CREATE TABLE canvas_items(
  id NUMBER NOT NULL,
  name VARCHAR2(50),
  note VARCHAR2(255),
  geometry SDO_GEOMETRY NOT NULL,
  item_type VARCHAR2(50) NOT NULL,
  id_photo NUMBER,
  CONSTRAINT fk_camera_photo FOREIGN KEY (id_photo) REFERENCES camera_photos(id_photo) on delete set null
);

ALTER TABLE canvas_items ADD CONSTRAINT PK_canvas_items PRIMARY KEY (id);
ALTER TABLE canvas_items ADD CONSTRAINT ENUM_item_type CHECK ( item_type IN ('Flower bed','Camera','Irrigation system','Building','Main path', 'Path', 'Tree','Bush','Green house','Lake') );

CREATE INDEX GEOMETRY_index ON canvas_items (geometry) indextype is MDSYS.SPATIAL_INDEX;

DELETE FROM USER_SDO_GEOM_METADATA WHERE
    TABLE_NAME = 'canvas_items' AND COLUMN_NAME = 'GEOMETRY';
INSERT INTO USER_SDO_GEOM_METADATA VALUES (
    'canvas_items', 'geometry',
    SDO_DIM_ARRAY(SDO_DIM_ELEMENT('X', 0, 650, 0.01), SDO_DIM_ELEMENT('Y', 0, 650, 0.01)),
    NULL
);


INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	1, 'house A',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 1),
		SDO_ORDINATE_ARRAY(75, 80, 150, 80, 150, 200, 75, 200, 75, 80)
	),
	'Green house'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	2, 'house B',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 1),
		SDO_ORDINATE_ARRAY(175, 80, 250, 80, 250,200, 175, 200, 175, 80)
	),
	'Green house'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	3, 'flower bed 1',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 1),
		SDO_ORDINATE_ARRAY(75, 230, 120, 230, 120, 300, 75, 300, 75, 230)
	),
	'Flower bed'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	4, 'flower bed 2',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 1),
		SDO_ORDINATE_ARRAY(125, 230, 170, 230, 170, 300, 125, 300, 125, 230)
	),
	'Flower bed'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	5, 'flower bed 3',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 1),
		SDO_ORDINATE_ARRAY(175, 230, 220, 230, 220, 300, 175, 300, 175, 230)
	),
	'Flower bed'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	6, 'flower bed 4',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 1),
		SDO_ORDINATE_ARRAY(225, 230, 270, 230, 270, 300, 225, 300, 225, 230)
	),
	'Flower bed'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	7, 'flower bed 5',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 1),
		SDO_ORDINATE_ARRAY(75, 330, 120, 330, 120, 400, 75, 400, 75, 330)
	),
	'Flower bed'
);



INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	8, 'flower bed 6',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 1),
		SDO_ORDINATE_ARRAY(125, 330, 170, 330, 170, 400, 125, 400, 125, 330)
	),
	'Flower bed'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	9, 'flower bed 7',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 1),
		SDO_ORDINATE_ARRAY(175, 330, 220, 330, 220, 400, 175, 400, 175, 330)
	),
	'Flower bed'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	10, 'flower bed 8',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 1),
		SDO_ORDINATE_ARRAY(225, 330, 270, 330, 270, 400, 225, 400, 225, 330)
	),
	'Flower bed'
);


INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	11, 'Main path 1',
	SDO_GEOMETRY(2002, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 2, 1),
		SDO_ORDINATE_ARRAY(810,50, 600,50, 500,150, 500, 300, 300,500, 150, 500, 0, 500, 150, 500,  150, 697)
	),
	'Main path'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (

	12, 'Lake 1',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 4),
		SDO_ORDINATE_ARRAY(400,150, 400,250, 300,200)
	),
	'Lake'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	13, 'Building1',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 1),
		SDO_ORDINATE_ARRAY(575, 500, 675, 500, 675, 700, 575, 700, 575, 500)
	),
	'Building'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	14, 'trees 1',
	SDO_GEOMETRY(2005, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1,1,1, 3,1,1, 5,1,1, 7,1,1, 9,1,1, 11,1,1, 13,1,1, 15,1,1, 17,1,1, 19,1,1),
		SDO_ORDINATE_ARRAY(705,650, 705,635, 705,620, 705,605, 705,590, 735,650, 735,635, 735,620, 735,605, 735,590)
	),
	'Tree'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	15, 'bushes 1',
	SDO_GEOMETRY(2005, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1,1,1, 3,1,1, 5,1,1, 7,1,1, 9,1,1, 11,1,1),
		SDO_ORDINATE_ARRAY(350,500, 375,475, 400,450, 425,425, 450,400, 475,375)
	),
	'Bush'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	16, 'Path 1',
	SDO_GEOMETRY(2002, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 2, 1),
		SDO_ORDINATE_ARRAY(280, 215, 280, 60, 50,60, 50, 215, 280,215, 280, 470)
	),
	'Path'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	17, 'Path 2',
	SDO_GEOMETRY(2002, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 2, 1),
		SDO_ORDINATE_ARRAY(280, 530, 280, 640, 568, 640)
	),
	'Path'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	18, 'Path 3',
	SDO_GEOMETRY(2002, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 2, 1),
		SDO_ORDINATE_ARRAY(650, 493, 650, 80)
	),
	'Path'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (

	19, 'Irrigator 1',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 4),
		SDO_ORDINATE_ARRAY(720,555,720,673,779,614)
	),
	'Irrigation system'
);


INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	20, 'Camera 1',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 1),
		SDO_ORDINATE_ARRAY(438,49,458,49,458,69,438,69,438,49)
	),
	'Camera'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	21, 'Camera 2',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 1),
		SDO_ORDINATE_ARRAY(18,49,38,49,38,69,18,69,18,49)
	),
	'Camera'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	22, 'Camera 3',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 1),
		SDO_ORDINATE_ARRAY(18,449,38,449,38,469,18,469,18,449)
	),
	'Camera'
);


INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (
	23, 'Camera 4',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 1),
		SDO_ORDINATE_ARRAY(548,659,568,659,568,679,548,679,548,659)
	),
	'Camera'
);

INSERT INTO canvas_items(id, name, geometry, item_type) VALUES (

	24, 'Irrigator 2',
	SDO_GEOMETRY(2003, NULL, NULL,
		SDO_ELEM_INFO_ARRAY(1, 1003, 4),
		SDO_ORDINATE_ARRAY(170,156,170,460,332,308)
	),
	'Irrigation system'
);


COMMIT;